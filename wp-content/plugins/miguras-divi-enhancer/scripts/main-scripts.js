function divienhancer_additional_scripts() {

  /*================= GENERAL VARIABLES ================*/




  /*================================== BREADCRUMB ============================*/
  /*============================================================================*/
  function de_breadcrumb() {
    jQuery(function ($) {

      $('.de-breadcrumb').each(function () {

        var $this = $(this);
        var $parentHeight = $this.parents('.et_pb_module_inner').outerHeight();
        var $parentWidth = $this.parents('.et_pb_module_inner').outerWidth();
        var $item = $this.find('.de-crumb-item');
        var $itemnumber = $this.find('.de-crumb-item').length;
        var $maxWidthItem = $parentWidth / $itemnumber;
        var $delimiter = $this.find('.divienhancer-crumb-delimiter');
        var $styles = '';
        var $itembackground = $this.attr('data-background');
        var $itembackgroundhover = $this.attr('data-backgroundhover');
        var $id = $this.attr('id');
        if ($id == '' || typeof $id == 'undefined') {
          $this.attr('id', 'de-breadcrumb' + Math.floor((Math.random() * 1000) + 1) + Math.random().toString(36).substring(2, 15));
          $id = $this.attr('id');
        }

        if ($this.hasClass('de-crumb-basic')) {
          $item.css({ maxWidth: $maxWidthItem });
          $styles += '<style class="de-breadcrumb-styles" type="text/css">';
          $styles += '#' + $id + '.de-crumb-basic span.divienhancer-crumb-delimiter.et-pb-icon {font-size: inherit;} ';
          $styles += '#' + $id + '.de-crumb-basic span.divienhancer-crumb-delimiter.et-pb-icon {position: relative;} ';
          $styles += '#' + $id + '.de-crumb-basic span.divienhancer-crumb-delimiter.et-pb-icon {top: 0.15em;} ';
          $styles += '#' + $id + '.de-crumb-basic span.divienhancer-crumb-delimiter.et-pb-icon {padding: 0 0.5em;} ';
          $styles += '</style>';




        }
        else {
          $item.css({ maxWidth: '' });
        }

        if ($this.hasClass('de-crumb-styleone')) {
          $styles += '<style class="de-breadcrumb-styles" type="text/css">';
          $styles += '#' + $id + '.de-crumb-styleone .de-crumb-item:after {background-color:' + $itembackground + ';} ';
          $styles += '#' + $id + '.de-crumb-styleone .de-crumb-item {background-color:' + $itembackground + ';} ';
          $styles += '#' + $id + '.de-crumb-styleone .de-crumb-item:hover {background-color:' + $itembackgroundhover + ';} ';
          $styles += '#' + $id + '.de-crumb-styleone span.de-crumb-item {background-color:' + $itembackgroundhover + ';} ';
          $styles += '#' + $id + '.de-crumb-styleone span.de-crumb-item:after {background-color:' + $itembackgroundhover + ';} ';
          $styles += '#' + $id + '.de-crumb-styleone .de-crumb-item:hover:after {background-color:' + $itembackgroundhover + ';} ';
          $styles += '</style>';
        }

        $this.find('.de-breadcrumb-styles').remove();
        $this.append($styles);


      })



    })
  }


  /*================================= DIVIENHANCER BING MAP =====================================*/



  function divienhancer_bing_map_script(origin) {
    if(jQuery('.divienhancer_bing_map').length > 0){


      let deBingKey = divienhancerData.bingKey;
      
      window.deBingMaps = [];
    
      let bingMap = document.createElement("script");
      let documentHead = document.getElementsByTagName("head")[0];
    
      bingMap.src = `https://www.bing.com/api/maps/mapcontrol?key=${deBingKey}`;
      bingMap.type = "text/javascript";
      bingMap.async = true;
      bingMap.defer = true;
      documentHead.appendChild(bingMap);
    
    
      let bingMapLoader = () => {
    
    
        jQuery('.divienhancer_bing_map').each(function () {
          var self = jQuery(this);
          self.attr('data-id', `de-bing-map-${Math.floor((Math.random() * 1000) + 1) + Math.random().toString(36).substring(2, 15)}`);
          var dataId = self.attr("data-id");
          self.find('.divienhancer_bing_map_load').attr('id', dataId)
          var mapid = jQuery(this).find('.divienhancer_bing_map_load').attr('id');
          var zoom = jQuery(this).attr('data-zoom');
          var maptypedata = jQuery(this).attr('data-type');
          var maptype = 'Microsoft.Maps.MapTypeId.' + maptypedata;
          var mapcenter = jQuery(this).attr("data-address")

    
    
    
          if (Microsoft) {
            window.deBingMaps[mapid] = new Microsoft.Maps.Map(document.getElementById(mapid), {
            });
            window.deBingMaps[mapid].setView({
              zoom: parseInt(zoom),
              mapTypeId: eval(maptype)
            });
    
    
            if(window.deBingMaps[mapid]){
              
    
              Microsoft.Maps.loadModule('Microsoft.Maps.Search', function () {
    
                self.attr("data-valid", "Invalid Address");
        
                var searchManager = new Microsoft.Maps.Search.SearchManager(window.deBingMaps[mapid]);
                var requestOptions = {
                  bounds: window.deBingMaps[mapid].getBounds(),
                  where: mapcenter,
                  callback: function (answer, userData) {
                    self.attr("data-valid", "Valid Address")
                    jQuery(".divienhancer_bing_map_loading").css({display: "none"});
                    window.deBingMaps[mapid].setView({ 
                      center: answer.results[0].location,
                      mapTypeId: eval(maptype)
                    });
                    
                  }
                };
                searchManager.geocode(requestOptions);
              });
    
    
            }
            
            
            
          }
    
    
        });
    
    
    
        jQuery('.divienhancer_bing_map_pin').each(function () {
          let self = jQuery(this);
          let parentid = jQuery(this).parents('.divienhancer_bing_map').attr('data-id');
          let mapType = jQuery(this).parents('.divienhancer_bing_map').attr('data-type');
          let map = window.deBingMaps[parentid];
          let address = jQuery(this).attr("data-address");
          let title = jQuery(this).attr("data-title");
    
          
          Microsoft.Maps.loadModule('Microsoft.Maps.Search', function () {
        
    
            var searchManager = new Microsoft.Maps.Search.SearchManager(map);
            var requestOptions = {
              bounds: map.getBounds(),
              where: address,
              callback: function (answer, userData) {
        
                let pinAddress = answer.results[0].location;
              
                let pin = new Microsoft.Maps.Pushpin(pinAddress, {
                  title: title,
                });
                map.entities.push(pin);

                // function to display pin content on click
                Microsoft.Maps.Events.addHandler(pin, 'click', function (e) { 
                  let pinLeft = e.point.x;
                  let pinTop = e.point.y;
                  let pinWrapper = self.parents('.divienhancer_bingMapChild');
                  
                  //hide all pins before display the clicked one
                  jQuery('.divienhancer_bingMapChild').css({display: "none"});

                  // display pin before get height, then get height to adjust position
                  pinWrapper.css({display: "block"});
                  self.parents('.divienhancer_bingMapChild').find('.divienhancer_bing_map_pin_content').css({display: "block"});
                  let contentHeight = pinWrapper.outerHeight();
                  pinWrapper.css({left: (pinLeft - 40), top: (pinTop - contentHeight - 30)});
                });


                //function to hide pin content when users moves the map
                Microsoft.Maps.Events.addHandler(map, 'viewchange', function () { 
                  self.parents('.divienhancer_bingMapChild').css({display: "none"});
                });

                jQuery('.divienhancer_bing_map_pin_close').on("click", function(){
                  self.parents('.divienhancer_bingMapChild').css({display: "none"});
                })


              }
            };
            searchManager.geocode(requestOptions);
          });
    
    
        });
    

      }
    
    
      if (origin == "front") {
        window.addEventListener("load", function () {
          bingMapLoader();
        });
      }
      else if (origin == "back") {
        setTimeout(function () {
          bingMapLoader();
        }, 2000);
      }
    }
  } // end of divienhancer_bing_map_script



  return {
    de_breadcrumb: de_breadcrumb,
    divienhancer_bing_map_script: divienhancer_bing_map_script
  }
} 






jQuery(document).ready(function () {
  divienhancer_additional_scripts().de_breadcrumb();
  divienhancer_additional_scripts().divienhancer_bing_map_script("front");

});


