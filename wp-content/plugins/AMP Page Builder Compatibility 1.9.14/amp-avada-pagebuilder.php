<?php

//Fusion(Avada) builder

class AMP_PC_Avada_Pagebuidler
{
    public $fusionBuilderObj;
    public function __construct()
    {
        $this->load_dependencies();
        //$this->define_public_hooks();
    }

    public function load_dependencies(){
        if(pagebuilder_for_amp_utils::get_setting('pagebuilder-for-amp-avada-support') ){
            //$this->fusionBuilderObj = FusionBuilder();
            add_filter('amp_post_template_css', [$this,'amp_divi_custom_styles'],7);
            add_filter('ampforwp_body_class', [$this,'ampforwp_body_class_avada'],11);
            add_action('pre_amp_render_post', array($this, 'load_fusion_builder_shortcodes'), 999);//fusion_builder_shortcodes_init
            
           require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'/parser/index.php';
        }
    }

    public static function classesReplacements($completeContent){
        $completeContent = preg_replace("/var\(--(.*?)\)/", "", $completeContent);
        $completeContent = preg_replace("/:root{[^{}]*}/", "", $completeContent);
        
        $completeContent = preg_replace("/fusion-clearfix/", "fclfx", $completeContent);
        $completeContent = preg_replace("/fusion-layout-column/", "fulcol", $completeContent);
        $completeContent = preg_replace("/fusion_builder_column/", "fublcol", $completeContent);
        
        $completeContent = preg_replace("/fusion-column-inner-bg-image/", "fucibimg", $completeContent);
        $completeContent = preg_replace("/fusion-column-inner-bg/", "fucoibg", $completeContent);
        $completeContent = preg_replace("/fusion-/", "fu-", $completeContent);
        $completeContent = preg_replace("/--fu-audio-(.*?);/", "    ", $completeContent);
        $completeContent = preg_replace("/fu-builder-column/", "fublcol", $completeContent);
        $completeContent = preg_replace("/content-box-column/", "conbxcol", $completeContent);
        $completeContent = preg_replace("/content-box-heading/", "con-bx-hg", $completeContent);
        $completeContent = preg_replace("/content-wrapper/", "con-wrp", $completeContent);
        $completeContent = preg_replace("/fu-content-boxes/", "fuconbxs", $completeContent);
        $completeContent = preg_replace("/fu-li-item-content/", "fu-li-it-con", $completeContent);
        $completeContent = preg_replace("/content-boxes/", "con-bxs", $completeContent);
        $completeContent = preg_replace("/nonhundred-percent-fullwidth/", "nonhnd-pnt-flwd", $completeContent);
        $completeContent = preg_replace("/non-hundred-percent-height-scrolling/", "non-hnd-pnt-hgt-scrl", $completeContent);
        $completeContent = preg_replace("/fu-builder-row/", "fublr", $completeContent);
        //$completeContent = preg_replace("/reading-box/", "rd-bx", $completeContent);
        $completeContent = preg_replace("/fu-fullwidth/", "fu-flwd", $completeContent);
        $completeContent = preg_replace("/fu-reading-box-container/", "fu-rd-bx-con", $completeContent);
        $completeContent = preg_replace("/fu-google/", "fu-ggl", $completeContent);
        $completeContent = preg_replace("/fullwidth-box/", "flwd-bx", $completeContent);
        $completeContent = preg_replace("/fu-button-text/", "fubt", $completeContent);
        $completeContent = preg_replace("/fu-carousel-nav/", "fucrn", $completeContent);
        $completeContent = preg_replace("/fu-image-wrapper/", "fuimwr", $completeContent);
        $completeContent = preg_replace("/fu-product-buttons/", "fuprb", $completeContent);
        $completeContent = preg_replace("/fu-carousel-title-below-image/", "fuctbim", $completeContent);
        //$completeContent = preg_replace("/reading-box/", "rd-bx", $completeContent);
        $completeContent = preg_replace("/fu-title/", "fu-t", $completeContent);
        $completeContent = preg_replace("/fu-one/", "fu-1", $completeContent);
        $completeContent = preg_replace("/title-heading/", "tl-hdg", $completeContent);
        $completeContent = preg_replace("/fu-page-title/", "fu-pg-tl", $completeContent);
        $completeContent = preg_replace("/fu-content-box-hover/", "fu-c-b-h", $completeContent);
        $completeContent = preg_replace("/icon-wrapper-hover-animation-pulsate/", "i-w-h-an", $completeContent);
        $completeContent = preg_replace("/fu-highlighted/", "fu-hl", $completeContent);
        $completeContent = preg_replace("/content-box-wrapper/", "c-bx-w", $completeContent);
        //$completeContent = preg_replace("/fontawesome-icon/", "ftawe-ic", $completeContent);
        $completeContent = preg_replace("/fu-button/", "fu-btn", $completeContent);
        //$completeContent = preg_replace("/fu-checklist/", "fu-ckli", $completeContent);
        $completeContent = preg_replace("/fu-column-wrapper/", "fucowr", $completeContent);
        $completeContent = preg_replace("/fu-rollover/", "furol", $completeContent);
        $completeContent = preg_replace("/fu-carousel/", "fucarl", $completeContent);
        $completeContent = preg_replace("/amp-wp-inline/", "amwpin", $completeContent);
        $completeContent = preg_replace('/.modal{display:none;/m', '.modal{display:block;' , $completeContent);
        $completeContent = preg_replace("/fu-columns/", "fu-cols", $completeContent);
        $completeContent = preg_replace("/fu-column/", "fu-col", $completeContent);
        $completeContent = preg_replace('/.fade{opacity:0;/m', '.fade{opacity:1;' , $completeContent);
        $completeContent = preg_replace('/fontawesome-icon/', 'fa-i' , $completeContent);        
        $completeContent = preg_replace('/fu-checklist/', 'fu-cl' , $completeContent);      
        $completeContent = preg_replace('/-o-background-size:cover;/', '' , $completeContent);      
        $completeContent = preg_replace('/-moz-background-size:cover;/', '' , $completeContent);      
        $completeContent = preg_replace('/-webkit-background-size:cover;/', '' , $completeContent);
        $completeContent = apply_filters("amp_pc_avada_css_sorting", $completeContent);
        return $completeContent;
    }
    function dirScan($avcssdir, $fullpath = false){
        $avcsspath = Avada::$template_dir_url . '/assets/css/media';
        $ignore = array(".","..");
        if (isset($avcssdir) && is_readable($avcssdir)){
            $dlist = array();
            $avcssdir = realpath($avcssdir);
            $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($avcssdir,RecursiveDirectoryIterator::KEY_AS_PATHNAME),RecursiveIteratorIterator::SELF_FIRST, RecursiveIteratorIterator::CATCH_GET_CHILD);
            foreach($objects as $entry){    
                    if(!in_array(basename($entry), $ignore)){
                        if (!$fullpath){
                            $entry = str_replace($avcssdir, '', $entry);
                        }           
                            $dlist[] = $avcsspath.$entry;
                    }                        
            }      
            return $dlist;
        }
    }
    function amp_divi_custom_styles(){
        $postId = get_the_ID();
        if( function_exists('ampforwp_is_front_page') && ampforwp_is_front_page() ){
            $postId = ampforwp_get_frontpage_id();
        }
        if ( 'active' !== get_post_meta( $postId, 'fusion_builder_status', true ) ) {
            return false;
        }
        $css = '';
        $css .= apply_filters( 'fusion_dynamic_css', '' );

        $dynamic_css_obj = Fusion_Dynamic_CSS::get_instance();
        $mode = ( method_exists( $dynamic_css_obj, 'get_mode' ) ) ? $dynamic_css_obj->get_mode() : $dynamic_css_obj->mode;

        $min_version = ( true == FUSION_BUILDER_DEV_MODE ) ? '' : '.min';
        $avcssdir = get_stylesheet_directory() . '/assets/css/media';
        
        $srcs = $this->dirScan($avcssdir);
        $srcs[] = Avada::$template_dir_url . '/assets/css/style.min.css';
        if ( is_rtl() && 'file' !== $this->compiler_mode ) {
			$srcs[] = Avada::$template_dir_url . '/assets/css/rtl.min.css';
        }
        
        if ( 'file' === $mode) {
            $Fusion_Dynamic_CSS_FileObj = new Fusion_Dynamic_CSS_File( $dynamic_css_obj );
            $srcs[] = $Fusion_Dynamic_CSS_FileObj->file( 'uri' );
		}else{
            $css = $dynamic_css_obj->make_css();
        }

        $update_css = pagebuilder_for_amp_utils::get_setting_data('avadaCssKeys');
		if($update_css!='' && strpos($update_css, ',')!==false){
			$csslinks = explode(",", $update_css);
			$csslinks = array_filter(array_map('trim', $csslinks));
			$srcs = array_merge($srcs, $csslinks);
		}elseif(filter_var($update_css, FILTER_VALIDATE_URL)){
			$srcs = array_merge($srcs, array($update_css));
		}
		if(count($srcs)>0){
			$srcs = array_unique($srcs);
		}

        //print_r($srcs);
        foreach ($srcs as $key => $urlValue) {
            $cssData = $this->ampforwp_remote_content($urlValue);
            $cssData = preg_replace("/\/\*(.*?)\*\//si", "", $cssData);
			$css .= preg_replace_callback('/url[(](.*?)[)]/', function($matches)use($urlValue){
                    $matches[1] = str_replace(array('"', "'"), array('', ''), $matches[1]);
                        if(!wp_http_validate_url($matches[1]) && strpos($matches[1],"data:")===false){
                            $urlExploded = explode("/", $urlValue);
                            $parentUrl = str_replace(end($urlExploded), "", $urlValue);
                            return 'url('.$parentUrl.$matches[1].")"; 
                        }else{
                            return $matches[0];
                        }
                    }, $cssData);
        }
        if(!empty(pagebuilder_for_amp_utils::get_setting_data('avadaCss-custom') ) ) {
            $css .= pagebuilder_for_amp_utils::get_setting_data('avadaCss-custom');
        }
        //Customizer css
        if( function_exists('wp_get_custom_css') ){
			$css .= wp_get_custom_css();
        }
        $css = str_replace(array(" img", " video", "!important"), array(" amp-img", " amp-video", ""), $css);

        echo $css .'
        body .hundred-percent-fullwidth .fu-row{
            max-width: none;
        }
		.fusion-image-hovers .hover-type-zoomin{
              overflow: visible;
         }
       .fusion-image-hovers .hover-type-zoomin:hover .fusion-column-inner-bg-image{
			transform: none;
       }
       fusion-column-inner-bg-image amp-img{
           width:100%;
           height:100%;
       }
       .modal-dialog {
        margin-top: 220px;
       }
       .modal{overflow:hidden;}
       .modal-dialog .toggle-alert {
        display: none;
       }
        ';
        
        
    }

    //if ( Avada()->settings->get( 'status_fontawesome' ) ) {
		// 	if ( 'off' === Avada()->settings->get( 'css_cache_method' ) ) {
		// 		wp_enqueue_style( 'fontawesome', FUSION_LIBRARY_URL . '/assets/fonts/fontawesome/font-awesome.min.css', array(), self::$version );
		// 	}

		// 	wp_enqueue_style( 'avada-IE-fontawesome', FUSION_LIBRARY_URL . '/assets/fonts/fontawesome/font-awesome.min.css', array(), self::$version );
		// 	wp_style_add_data( 'avada-IE-fontawesome', 'conditional', 'lte IE 9' );
		// }

    public function ampforwp_remote_content($src){
        if($src){
            $arg = array( "sslverify" => false, "timeout" => 60 ) ;
            $response = wp_remote_get( $src, $arg );
            if ( wp_remote_retrieve_response_code($response) == 200 && is_array( $response ) ) {
              $header = wp_remote_retrieve_headers($response); // array of http header lines
              $contentData =  wp_remote_retrieve_body($response); // use the content
              return $contentData;
            }else{
                $contentData = file_get_contents( $src );
                if(! $contentData ){
                    $data = str_replace(get_site_url(), '', $src);//content_url()
                    $data = getcwd().$data;
                    if(file_exists($data)){
                        $contentData = file_get_contents($data);
                    }
                }
                return $contentData;
            }

        }
        return '';
    }

    public function ampforwp_body_class_avada($classes){
        $classes = apply_filters( 'body_class', $classes);
        $classes = FusionBuilder()->body_class_filter($classes);
        return $classes;
    }
    function load_fusion_builder_shortcodes(){
        $url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH),'/' );
	  	$explode_path = explode('/', $url_path);
        if ( in_array('amp', $explode_path) || ampforwp_is_amp_endpoint()) {
            if ( class_exists( 'FusionBuilder' ) ) {
                $filesArray = array('fusion-image-carousel.php', 'fusion-column.php', 'fusion-google-map.php', 'fusion-woo-product-slider.php','fusion-button.php','fusion-modal.php', 'fusion-content-boxes.php','fusion-title.php','fusion-tagline.php','fusion-checklist.php');
                foreach ($filesArray as $key => $value) {
                    if(file_exists(AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR."/includes/avada/".$value)){
                        require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR."/includes/avada/".$value;
                    }
                }
            }
        }
    }

    public static function load_ajax_calls(){

    }
}

/**
* Admin section portal Access
**/
//add_action('plugins_loaded', 'pagebuilder_for_amp_avada_option');
pagebuilder_for_amp_avada_option();
function pagebuilder_for_amp_avada_option(){
    if(is_admin()){
        new pagebuilder_for_amp_avada_Admin();
    }else{
        // Instantiate AMP_PC_Avada_Pagebuidler.
        $diviAmpBuilder = new AMP_PC_Avada_Pagebuidler();
    }
    if ( defined( 'DOING_AJAX' ) ) {
        AMP_PC_Avada_Pagebuidler::load_ajax_calls();
    }
    
}
Class pagebuilder_for_amp_avada_Admin{
    function __construct(){
        add_filter( 'redux/options/redux_builder_amp/sections', array($this, 'add_options_for_avada'),7,1 );
    }
    public static function get_admin_options_avada($section = array()){
        $obj = new self();
        $section = $obj->add_options_for_avada($section);
        return $section;
    }
    function add_options_for_avada($sections){
        $desc = 'Enable/Activate Avada pagebuilder';
        $theme = wp_get_theme(); // gets the current theme
        if ( class_exists( 'FusionBuilder' ) ) {
            $desc = '';
        }
       // print_r( $sections[3]['fields']);die;
        $accordionArray = array();
        $sectionskey = 0;
        foreach ($sections as $sectionskey => $sectionsData) {
            if($sectionsData['id']=='amp-content-builder' &&  count($sectionsData['fields'])>0 ){
                foreach ($sectionsData['fields'] as $fieldkey => $fieldvalue) {
                    if($fieldvalue['id'] == 'ampforwp-avada-pb-for-amp-accor'){
                        $accordionArray = $sections[$sectionskey]['fields'][$fieldkey];
                         unset($sections[$sectionskey]['fields'][$fieldkey]);
                    }
                    if($fieldvalue['id'] == 'ampforwp-avada-pb-for-amp'){
                        unset($sections[$sectionskey]['fields'][$fieldkey]);
                    }
                }
                break;
            }
        }
        $sections[$sectionskey]['fields'][] = $accordionArray;
        $sections[$sectionskey]['fields'][] = array(
                               'id'       => 'pagebuilder-for-amp-avada-support',
                               'type'     => 'switch',
                               'title'    => esc_html__('AMP Avada (Fusion builder) (BETA)','accelerated-mobile-pages'),
                               'tooltip-subtitle' => esc_html__('Enable or Disable the avada for AMP', 'accelerated-mobile-pages'),
                               'desc'     => $desc,
                               'section_id' => 'amp-content-builder',
                               'default'  => false
                            );
        foreach ($this->amp_divi_fields() as $key => $value) {
            $sections[$sectionskey]['fields'][] = $value;
        }
        

        return $sections;

    }

    public function amp_divi_fields(){
        $contents[] = array(
                        'id'       => 'avadaCssKeys',
                        'type'     => 'textarea',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Enter css url', 'amp-pagebuilder-compatibility'),
                        'subtitle'  => esc_html__('', 'amp-pagebuilder-compatibility'),
                        'default'  => '',
                        'desc'      => esc_html__( 'Add your css url in comma saperated', 'amp-pagebuilder-compatibility' ),
                       // 'required'=> array(array('pagebuilder-for-amp-wpbakery-support','==', 1)),
                         'section_id' => 'amp-content-builder',

                    );
        $contents[] = array(
                        'id'       => 'avadaCss-custom',
                        'type'     => 'textarea',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Enter custom css', 'amp-pagebuilder-compatibility'),
                        'subtitle'  => esc_html__('', 'amp-pagebuilder-compatibility'),
                        'default'  => '',
                        'desc'      => esc_html__( 'Add your custom css code', 'amp-pagebuilder-compatibility' ),
                         'section_id' => 'amp-content-builder',
                    );

        /*$contents[] = array(
                        'id'       => 'avada_fontawesome_support',
                        'type'     => 'switch',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Load fontawesome', 'amp-pagebuilder-compatibility'),
                        'desc'      => esc_html__( 'Load fontawesome library from CDN', 'amp-pagebuilder-compatibility' ),
                        'default'  => 0,
                        //'required'=> array(array('pagebuilder-for-amp-wpbakery-support','==', 1)),
                         'section_id' => 'amp-content-builder',
                    );*/
        return $contents;
    }
}