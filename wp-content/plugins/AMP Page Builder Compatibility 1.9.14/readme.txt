=== Page Builder For AMP ===
Contributors: AMPforWP Team
Requires at least: 3.0
Tested up to: 5.2.1
Stable tag: 1.9.14
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Page Builder Functionality for AMP in WordPress. Most easiest and the best way to include Page Builder Compatibility in AMP.

== Changelog ==	

= 1.9.14 (17 January 2020) =
* Features Added:- =
* Image gallery module from Ultimate Addons for Elementor(Plugin) support Added- Elementor
* Circle Counter Module support Added- Divi
* Rehub Module's Pros,Cons,Ratebar support Added- Elementor
* Bugs Fixed:- =
* Image and video stretching in Avia Pagebuilder, issue resolved.
* Image URL changing and stretching in Elementor, issue resolved.
* Gallery module not working issue resolved- Divi
* Optimizations done for Avada.
* Image URL changed due to CSS optimization in Elementor, issue resolved.
* Tree shaking issue in Divi with Woocommerce, issue resolved.
* Image carousel images goes out of container when custom link is set, issue resolved - Elementor
* Gallery Module of Elementor Pro not working, issue resolved.
* Condition added for v2 layout to load CSS.
* Warning getimagesize function, issue resolved - Divi
* Need to add a 'clear CSS button' when the user is using WP-bakery, added.
* Fallback issue in popup gallery elementor, resolved.

= 1.9.13 (31 December 2019) =
* Features Added:- =
* FullWidth Slider Support Added - Divi
* Tab Module Support Added - Elementor
* Gallery Module Support Added - Elementor PRO 
* Blurb Module Support Added - Divi
* Image Module Support Added - Divi
* Added all three font types for Font Awesome icons - Elementor
* Bugs Fixed:- =
* Image distortion issue in when aligned Center, resolved - Avia.
* Updated all Sabberwarm files and Fatal error: Call to Undefined function iconv resolved.
* Form getting stripped when SASWP is active, issue resolved in Elementor and Divi.

= 1.9.12 (13 December 2019) =
* Features Added:- =
* Blog Module Support Added - Divi.
* Connected Basic Gallery module with AMP Option Panel - Elementor.
* Bugs Fixed:- =
* Elementor's image carousel support improved.
* Fatal error Uncaught Error: Class 'ElementorForAmp\Widgets\Shapes' not found resolved.

= 1.9.11 (06 December 2019) =
* Features Added:- =
* Fusion Checklist, Content-boxes, Tagline, Text, Title Support Added.
* Background Video Support- Elementor
* Basic Gallery connected with AMP Options Panel - Elementor
* Bugs Fixed:- =
* Footer content design issue in Elementor resolved.
* Css Optimizations done in Elementor.
* Image Gallery support improved.
* Minor CSS fix done for Products Module.
* Fatal error in Divi Number Counter Module issue resolved.

= 1.9.10 (22 November 2019) =
* Features Added:-
* Gallery Module Support Added- Divi.
* Media Carousel Module Support Added- Elementor.
* Bugs Fixed:-
* Elementor Page title displays only on Default Template issue resolved, connected with Option Panel to show/Hide Title.
* Portfolio Images size issue resolved.
* Depreciated:
* WPBakery Support depreciated.

= 1.9.9 (15 November 2019) =
* Feature Added:-
* Image Gallery Support Added- WPBakery.
* Bugs Fixed:-
* Images coming from the theme's CSS not rendering in AMP, issue resolved.
* Avada theme's media CSS files not rendering in AMP, fetched all the media files.
* Elementor's Internal Embedding CSS Print method Option not rendering CSS Styling in AMP version, issue resolved(fetched CSS).

= 1.9.8 (01 November 2019) =
* Bugs Fixed:-
* Fecthing CSS with the proper Domain name done in WP Bakery.
* CSS Optimization done in Divi, Elementor.
* Title appearing in Elementor Pages, removed and resolved.
* srcset="" Validation error occuring in Divi pages issue resolved.

= 1.9.7 (18 October 2019) =
* Features Added:-
* Divi's Button Module Support Added.
* Compatibility of Modal Box Module of Fusion Builder Added.
* Pages will be shown with Full Width in Divi.
* Bugs Fixed:-
* Extra Spacing in Footer, issue resolved.
* Parallex images not showing issue resolved.
* WPBakery not loading CSS, issue resolved.

= 1.9.6 (04 Oct 2019) =
* Feature Added:
* Divi's Shop Module Compatibility Added.
* Bugs Fixed:
* Star Rating not showing issue resolved.
* Elementor Slide Module not displaying, issue resolved.

= 1.9.5 (27 Sep 2019) =
* Features Added:
* Elementor's Product Module Support Added.
* Avia's Full Width Easy Slider Module Support Added.
* Code Block Module (HTML) Support Added.
* Bug Fixed:
* Fatal Error Call to Undefined function mb_check_encoding resolved.

= 1.9.4 (20 Sep 2019) =
* Features Added:
* Image Compatibility with Publisher theme's WP Bakery modules Added.
* Compatibility with Woocommerce Grid Module (Rehub Theme) Added.
* Added Font Awesome Free when class .fas, is found.
* Button Module Compatibility Added.
* Bugs Fixed:
* Multiple Menu occuring due to Raven Plugin of JupiterX theme,issue resolved.
* Countdown Timer support properly added.
* Divi Contact Form's Module Form not submitting, issue resolved.
* Post Module Skin Card Image issue resolved.
* Post Module skin Classic image issue resolved.

= 1.9.3 (09 Sep 2019) =
* Bug Fixed:
* Fatal error cannot re-declare same function name resolved.

= 1.9.2 (31 Aug 2019) =
* Features Added:
* Elementor Comment Module Support Added.
* Elementor's Contact Form Redirection Support Added.
* Shop Page CSS for Divi's Product Columns are not applying resolved.
* Added VC background video support for Row and Section.

= 1.9.1 (19 Aug 2019) =
* New Feature Added:
* Compatibility with Avia (Enfold). 
* Features Added:
* No Sidebar Support for Posts with Avia Builder Added.
* Template Mode support Added. For more information, you may view the below URL.
   (https://ampforwp.com/tutorials/article/how-to-create-a-custom-amp-wordpress-theme-with-level-up-framework/)
* Bugs Fixed:
* CSS Classes Minification improved.
* Unnecessary CSS eliminated in Elementor.
* Avada CSS improved.

= 1.9 (09 Aug 2019) =
* New Features Added:
* Compatibility with Avada (Fusion builder) (BETA). 
* Avada (Fusion builder) (BETA) Compatible with AMP by Automattic.
* Avada:
* Google Map Support Added.
* Woo Product Carousel support Added.
* Image Carousel support Added.
* Theme Customizer Support Added.
* Bugs Fixed:
* DIVI CSS reduced by eliminating Duplicate CSS URL's.
* Filter added to reduce CSS for all the Pagebuilders.
* CSS Parser CALC function improved.
* DIVI CSS was loading even if page wasn't built with it resolved.
* Custom CSS URL not working in DIVI resolved.
* CSS Classes Minification improved.

= 1.8 (02 Aug 2019) =
* New Feature Added:
* Compatibility with AMP by Automattic. 
* Elementor:
* Elementor Canvas Support added. 
* Customizer CSS Support added.
* Divi:
* FullWidth Post Slider Support Added.
* Email Optin Module Compatibilty Added.
* Facebook Comment Module Compatibility Added.
* Development Mode Support added for CSS.
* Customizer CSS Support added.
* Slider Module Support Added.
* WPBakery:
* Customizer CSS Support added.
* Bugs Fixed:
* Image Carousel Setting's Custom Height not working resolved.
* In Divi using a Text Module margin-bottom was added by default, resolved.
* CSS Minification improved.
* CSS URL not working when used with " , " in the Enter CSS Url Section resolved.
* File Fetch method of CSS improved.
* Image width of Post exceeds in Elementor resolved. 
* CSS URL fetch not working in Divi resolved.
* Testimonial Image Width issue resolved.
* Video Module giving Fatal error resolved.
* Animated Headline Content CSS improved.

= 1.7 (20 July 2019) =
* Minor improvements done.

= 1.6 (19 July 2019) =
* Elementor:
* Added Compatibility with Slides Module.
* Added Custom URL Support in Image Carousel.
* Added Rotating Text Compatibility.
* Added Video Module Compatibility.
* Divi:
* Added Compatibility with Slides Module.
* Bugs Fixed:
* Conflicts in Custom CSS URL's resolved.
* WPBakery CSS Minification improved and Blank Page issue resolved.
* Cached CSS will be cleared when a Post/Page is saved.
* Accordion Module not working resolved.
* Toggle Functionality improved.
* CSS loading when a Page/Post not built with Elementor, resolved.
* Font awesome support improved which adds Font Awesome 5 Brands.
* Fatal error resolved with the Divi Builder Plugin.

= 1.5 (13 July 2019) =
* Features Added:
* Added an Option to Enable/Disable the Page/Post Title for Full-Width Template.
* Improved performance of Treeshaking.
* Added an Option of Debug Mode.
* Added an Option to Enable/Disable Title or Comments in a Page/Post.
* Added an Option to Clear Cached CSS.
* Elementor:
* Added Compatibility with Share Button Module.
* Added Carousel, Media, Amp-Review Modules. 
* Added Swiper Module Support.
* Added an Option to Add/Remove Swift Header and Footer. 
* Added Image-Gallery Support.

 === Divi ===
 * Added Video Module support.
 * Added Numbercounter, Bar Counter, Bar Counter Item Modules Support.
  
 === Bugs Fixed ==
 * H1 Tag not rendering issue resolved.
 * Fatal error resolved with ampforwp_get_all_post_types.
 * Removed Lazy Load Class elementor-invisible.
 * Blacklist Sanitizer not loading in Elementor resolved.
 
= 1.4 (04 June 2019) =

* Bug Fixed:
    * Fatal error occuring when Elementor Pro isn't active resolved.

= 1.3 (31 May 2019) =

* Features Added:
    * Added Elementor Form Support.
    * Added Elementor Price-table module support.
    * Added CSS Support with plugins "JetBlog For Elementor", "JetElements For 
      Elementor","Ultimate Addons for Elementor" for Elementor page builder.
    * Added Toggle support of Elementor and Divi page builder.
    * Added Divi extended support with Contact form,Map,Tabs,FilterablePortfolio,
      Video modules.
    * Added fields for custom css changes in setting panel for Elementor.
	
* Bugs Fixed:
    * Resolved amp validation warning of multiple layout attribute in amp-img. 
    * Fixed amp carousel size issues of elementor page builder.
    * CSS minification for Elementor and Divi page builder designs.  

= 1.2 ( 3 May 2019) =

* Resolved Fatal error: Cannot redeclare pb_for_amp_plugin_update().
* Improved Divi Page Builder Design.
* Improved Elementor Page Builder Design.

= 1.1 ( 10 April 2019) =
* Initial features released
