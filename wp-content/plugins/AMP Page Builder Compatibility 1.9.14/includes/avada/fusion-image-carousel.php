<?php

if ( fusion_is_element_enabled( 'fusion_images' ) ) {

    if ( ! class_exists( 'AMP_PC_FusionSC_ImageCarousel' ) ) {
        /**
         * Shortcode class.
         *
         * @package fusion-builder
         * @since 1.0
         */
        class AMP_PC_FusionSC_ImageCarousel extends Fusion_Element {

            /**
             * Image Carousels counter.
             *
             * @access private
             * @since 1.0
             * @var int
             */
            private $image_carousel_counter = 1;

            /**
             * The image data.
             *
             * @access private
             * @since 1.0
             * @var false|array
             */
            private $image_data = false;

            /**
             * Parent SC arguments.
             *
             * @access protected
             * @since 1.0
             * @var array
             */
            protected $parent_args;

            /**
             * Child SC arguments.
             *
             * @access protected
             * @since 1.0
             * @var array
             */
            protected $child_args;

            /**
             * Constructor.
             *
             * @access public
             * @since 1.0
             */
            public function __construct() {
                parent::__construct();
                add_filter( 'fusion_attr_image-carousel-shortcode', array( $this, 'attr' ) );
                add_filter( 'fusion_attr_image-carousel-shortcode-carousel', array( $this, 'carousel_attr' ) );
                add_filter( 'fusion_attr_image-carousel-shortcode-slide-link', array( $this, 'slide_link_attr' ) );
                add_filter( 'fusion_attr_fusion-image-wrapper', array( $this, 'image_wrapper' ) );

                /*
                TODO:
                add_filter( 'fusion_attr_fusion-nav-prev', array( $this, 'fusion_nav_prev' ) );
                add_filter( 'fusion_attr_fusion-nav-next', array( $this, 'fusion_nav_next' ) );
                */

                add_shortcode( 'fusion_images', array( $this, 'render_parent' ) );
                add_shortcode( 'fusion_image', array( $this, 'render_child' ) );

                add_shortcode( 'fusion_clients', array( $this, 'render_parent' ) );
                add_shortcode( 'fusion_client', array( $this, 'render_child' ) );

            }
            
            function print_css_data(){
                echo '.fusion-carousel amp-carousel .slide {
                    width: 30%;
                    margin-right: 20px;
                }';
            }

            /**
             * Render the parent shortcode.
             *
             * @access public
             * @since 1.0
             * @param  array  $args    Shortcode parameters.
             * @param  string $content Content between shortcode.
             * @return string          HTML output.
             */
            public function render_parent( $args, $content = '' ) {
                add_action("amp_post_template_css", array($this,'print_css_data'));
                $defaults = FusionBuilder::set_shortcode_defaults(
                    array(
                        'hide_on_mobile' => fusion_builder_default_visibility( 'string' ),
                        'class'          => '',
                        'id'             => '',
                        'autoplay'       => 'no',
                        'border'         => 'yes',
                        'columns'        => '5',
                        'column_spacing' => '13',
                        'image_id'       => '',
                        'lightbox'       => 'no',
                        'mouse_scroll'   => 'no',
                        'picture_size'   => 'fixed',
                        'scroll_items'   => '',
                        'show_nav'       => 'yes',
                        'hover_type'     => 'none',
                    ),
                    $args,
                    'fusion_images'
                );

                $defaults['column_spacing'] = FusionBuilder::validate_shortcode_attr_value( $defaults['column_spacing'], '' );

                extract( $defaults );

                $this->parent_args = $defaults;

                $html  = '<div ' . FusionBuilder::attributes( 'image-carousel-shortcode' ) . '>';
                $html .= '<div ' . FusionBuilder::attributes( 'image-carousel-shortcode-carousel' ) . '>';
                $html .= '<div ' . FusionBuilder::attributes( 'fusion-carousel-positioner' ) . '>';

                // The main carousel.
                $html .= '<amp-carousel class="carousel2"
                layout="fixed-height"
                height="275"
                type="carousel" ' . FusionBuilder::attributes( 'fusion-carousel-holder' ) . '>';
                $html .= do_shortcode( $content );
                $html .= '</amp-carousel>';

                // Check if navigation should be shown.
                if ( 'yes' === $show_nav ) {
                    $html .= '<div ' . FusionBuilder::attributes( 'fusion-carousel-nav' ) . '>';
                    $html .= '<span ' . FusionBuilder::attributes( 'fusion-nav-prev' ) . '></span>';
                    $html .= '<span ' . FusionBuilder::attributes( 'fusion-nav-next' ) . '></span>';
                    $html .= '</div>';
                }
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';

                $this->image_carousel_counter++;

                return $html;

            }

            /**
             * Builds the attributes array.
             *
             * @access public
             * @since 1.0
             * @return array
             */
            public function attr() {

                $attr = fusion_builder_visibility_atts(
                    $this->parent_args['hide_on_mobile'],
                    array(
                        'class' => 'fusion-image-carousel fusion-image-carousel-' . $this->parent_args['picture_size'],
                    )
                );

                if ( 'yes' === $this->parent_args['lightbox'] ) {
                    $attr['class'] .= ' lightbox-enabled';
                }

                if ( 'yes' === $this->parent_args['border'] ) {
                    $attr['class'] .= ' fusion-carousel-border';
                }

                if ( $this->parent_args['class'] ) {
                    $attr['class'] .= ' ' . $this->parent_args['class'];
                }

                if ( $this->parent_args['id'] ) {
                    $attr['id'] = $this->parent_args['id'];
                }

                return $attr;

            }

            /**
             * Builds the carousel attributes array.
             *
             * @access public
             * @since 1.0
             * @return array
             */
            public function carousel_attr() {

                $attr['class']            = 'fusion-carousel';
                $attr['data-autoplay']    = $this->parent_args['autoplay'];
                $attr['data-columns']     = $this->parent_args['columns'];
                $attr['data-itemmargin']  = $this->parent_args['column_spacing'];
                $attr['data-itemwidth']   = 180;
                $attr['data-touchscroll'] = $this->parent_args['mouse_scroll'];
                $attr['data-imagesize']   = $this->parent_args['picture_size'];
                $attr['data-scrollitems'] = $this->parent_args['scroll_items'];
                return $attr;

            }

            /**
             * Render the child shortcode.
             *
             * @access public
             * @since 1.0
             * @param  array  $args   Shortcode parameters.
             * @param  string $content Content between shortcode.
             * @return string         HTML output.
             */
            public function render_child( $args, $content = '' ) {
                global $fusion_library;

                $defaults = FusionBuilder::set_shortcode_defaults(
                    array(
                        'alt'        => '',
                        'image'      => '',
                        'image_id'   => '',
                        'link'       => '',
                        'linktarget' => '_self',
                    ),
                    $args,
                    'fusion_image'
                );

                extract( $defaults );

                $this->child_args = $defaults;

                $width = $height = '';

                $image_size = 'full';
                if ( 'fixed' === $this->parent_args['picture_size'] ) {
                    $image_size = 'portfolio-two';
                    if ( '6' === $this->parent_args['columns'] || '5' === $this->parent_args['columns'] || '4' === $this->parent_args['columns'] ) {
                        $image_size = 'blog-medium';
                    }
                }

                $this->image_data = $fusion_library->images->get_attachment_data_by_helper( $this->child_args['image_id'], $image );

                $output = '';
                if ( $this->image_data['id'] ) {
                    if ( $alt ) {
                        $output = wp_get_attachment_image( $this->image_data['id'], $image_size, false, array( 'alt' => $alt ) );
                    } else {
                        $output = wp_get_attachment_image( $this->image_data['id'], $image_size );
                    }
                } else {
                    $output = '<img src="' . $image . '" alt="' . $alt . '"/>';
                }

                if ( 'no' === $this->parent_args['mouse_scroll'] && ( $link || 'yes' === $this->parent_args['lightbox'] ) ) {
                    $output = '<a ' . FusionBuilder::attributes( 'image-carousel-shortcode-slide-link' ) . '>' . $output . '</a>';
                }
                return '<div class="slide" ' . FusionBuilder::attributes( 'fusion-carousel-item' ) . '>' . $output . '</div>';
              //  return '<div class="slide" ' . FusionBuilder::attributes( 'fusion-carousel-item' ) . '><div ' . FusionBuilder::attributes( 'fusion-carousel-item-wrapper' ) . '><div ' . FusionBuilder::attributes( 'fusion-image-wrapper' ) . '>' . $output . '</div></div></div>';
            }

            /**
             * Builds the slide-link attributes array.
             *
             * @access public
             * @since 1.0
             * @return array
             */
            public function slide_link_attr() {

                $attr = array();

                if ( 'yes' === $this->parent_args['lightbox'] ) {

                    if ( ! $this->child_args['link'] ) {
                        $this->child_args['link'] = $this->child_args['image'];
                    }

                    $attr['data-rel'] = 'iLightbox[gallery_image_' . $this->image_carousel_counter . ']';

                    if ( $this->image_data ) {
                        $attr['data-caption'] = $this->image_data['caption'];
                        $attr['data-title'] = $this->image_data['title'];
                        $attr['aria-label'] = $this->image_data['title'];
                    }
                }

                $attr['href'] = $this->child_args['link'];

                $attr['target'] = $this->child_args['linktarget'];
                if ( '_blank' === $this->child_args['linktarget'] ) {
                    $attr['rel'] = 'noopener noreferrer';
                }
                return $attr;

            }

            /**
             * Builds the image-wrapper attributes array.
             *
             * @access public
             * @since 1.0
             * @return array
             */
            public function image_wrapper() {
                if ( $this->parent_args['hover_type'] ) {
                    return array(
                        'class' => 'fusion-image-wrapper hover-type-' . $this->parent_args['hover_type'],
                    );
                }
                return array(
                    'class' => 'fusion-image-wrapper',
                );
            }

            /**
             * Builds the "previous" nav attributes array.
             *
             * @access public
             * @since 1.0
             * @return array
             */
            public function fusion_nav_prev() {
                return array(
                    'class' => 'fusion-nav-prev fusion-icon-left',
                );
            }

            /**
             * Builds the "next" nav attributes array.
             *
             * @access public
             * @since 1.0
             * @return array
             */
            public function fusion_nav_next() {
                return array(
                    'class' => 'fusion-nav-next fusion-icon-right',
                );
            }

            /**
             * Builds the dynamic styling.
             *
             * @access public
             * @since 1.1
             * @return array
             */
            public function add_styling() {

                global $wp_version, $content_media_query, $six_fourty_media_query, $three_twenty_six_fourty_media_query, $ipad_portrait_media_query, $fusion_library, $fusion_settings, $dynamic_css_helpers;

                $elements = array(
                    '.fusion-carousel .fusion-carousel-nav .fusion-nav-prev',
                    '.fusion-carousel .fusion-carousel-nav .fusion-nav-next',
                );
                $css['global'][ $dynamic_css_helpers->implode( $elements ) ]['background-color'] = $fusion_library->sanitize->color( $fusion_settings->get( 'carousel_nav_color' ) );

                $css['global'][ $dynamic_css_helpers->implode( $elements ) ]['width'] = $fusion_library->sanitize->size( $fusion_settings->get( 'slider_nav_box_dimensions', 'width' ) );

                preg_match_all( '!\d+!', $fusion_settings->get( 'slider_nav_box_dimensions', 'height' ), $matches );
                $half_slider_nav_box_height = '' !== $fusion_settings->get( 'slider_nav_box_dimensions', 'height' ) ? $matches[0][0] / 2 . $fusion_library->sanitize->get_unit( $fusion_settings->get( 'slider_nav_box_dimensions', 'height' ) ) : '';

                $css['global'][ $dynamic_css_helpers->implode( $elements ) ]['height'] = $fusion_library->sanitize->size( $fusion_settings->get( 'slider_nav_box_dimensions', 'height' ) );
                $css['global'][ $dynamic_css_helpers->implode( $elements ) ]['margin-top'] = '-' . $half_slider_nav_box_height;

                $elements = array(
                    '.fusion-carousel .fusion-carousel-nav .fusion-nav-prev:before',
                    '.fusion-carousel .fusion-carousel-nav .fusion-nav-next:before',
                );

                $css['global'][ $dynamic_css_helpers->implode( $elements ) ]['line-height'] = $fusion_library->sanitize->size( $fusion_settings->get( 'slider_nav_box_dimensions', 'height' ) );

                $css['global'][ $dynamic_css_helpers->implode( $elements ) ]['font-size'] = $fusion_library->sanitize->size( $fusion_settings->get( 'slider_arrow_size' ) );

                return $css;
            }

            /**
             * Sets the necessary scripts.
             *
             * @access public
             * @since 1.1
             * @return void
             */
            public function add_scripts() {
                Fusion_Dynamic_JS::enqueue_script( 'fusion-lightbox' );
                Fusion_Dynamic_JS::enqueue_script( 'fusion-carousel' );
            }
        }
    }
    remove_shortcode( 'fusion_images');
    remove_shortcode( 'fusion_image');

    remove_shortcode( 'fusion_clients');
    remove_shortcode( 'fusion_client');
    new AMP_PC_FusionSC_ImageCarousel();
    
}