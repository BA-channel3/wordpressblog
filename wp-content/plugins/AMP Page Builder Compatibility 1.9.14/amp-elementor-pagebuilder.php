<?php 
final class Elementor_For_Amp {

	const MINIMUM_ELEMENTOR_VERSION = '2.0.0';

	const MINIMUM_PHP_VERSION = '5.0';
	public $postID;
	public $header_html;
	public $sanitizer_script;
	public $footer_html;

	public function __construct() {
		// Init Plugin
		 $this->initialize();
	}
	function initialize(){
		if(pagebuilder_for_amp_utils::get_setting('pagebuilder-for-amp-elementor-support') ){
			add_filter('amp_post_template_css', [$this,'amp_elementor_custom_styles'],11);
			add_filter('ampforwp_body_class', [$this,'ampforwp_body_class_elementor'],11);
			add_action('ampforwp_before_head', [$this,'elem_amp_fonts']);
			add_filter('ampforwp_pagebuilder_status_modify', [$this, 'pagebuilder_status_reset_elementor'], 10, 2);
			//blacklist sanitizer
			add_filter('ampforwp_content_sanitizers',[$this, 'ampforwp_blacklist_sanitizer'], 99);
			add_filter('amp_content_sanitizers',[$this, 'ampforwp_blacklist_sanitizer'], 99);

			//PDF embedder
			add_action('pre_amp_render_post' ,[$this, 'ampforwp_pdf_embedder_compatibility'] );
			if (class_exists('\ElementorPro\Modules\Forms\Classes\Recaptcha_Handler') && \ElementorPro\Modules\Forms\Classes::is_enabled() ) {
				$obj = new \ElementorPro\Modules\Forms\Classes\Recaptcha_Handler();
				remove_action( 'elementor_pro/forms/validation', [ $obj, 'validation' ], 10, 2 );
				add_action( 'elementor_pro/forms/validation', [ $this, 'recaptcha_validation' ], 10, 2 );
			}
			add_action('wp_ajax_elementor_amp_cform_submission',[$this, 'elementor_amp_cform_submission']);
			add_action('wp_ajax_nopriv_elementor_amp_cform_submission',[$this,'elementor_amp_cform_submission']);

			add_action( 'wp_ajax_amp_elementor_ajax_comment',[$this,'amp_elementor_ajax_comment']); 
            add_action( 'wp_ajax_nopriv_amp_elementor_ajax_comment', [$this,'amp_elementor_ajax_comment'] ); 

			//include files
			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'/load-elementor-widgets.php' );
			require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'/parser/index.php';
		}
	}
	

	function pagebuilder_status_reset_elementor($response, $postId ){
  		if(class_exists('\Elementor\Plugin') && \Elementor\Plugin::$instance->db->is_built_with_elementor($postId) ){
  			//$response = true;
  		}
        return $response;
    }

    public static function classesReplacements($completeContent){
		$completeContent = preg_replace("/<form class=\"elementor-search-form\" role=\"search\" action=\"(.*?)\" method=\"get\">(.*?)<\/form>/s", "", $completeContent);
		$completeContent = preg_replace("/elementor-invisible/", "", $completeContent);
    	$completeContent = preg_replace("/elementor-element-populated/", "epo", $completeContent);
    	$completeContent = preg_replace("/elementor-element/", "ee", $completeContent);    	      
    	$completeContent = preg_replace("/elementor-container/", "econ", $completeContent);    	
    	$completeContent = preg_replace("/elementor-row/", "er", $completeContent);
    	$completeContent = preg_replace("/elementor-default/", "ed", $completeContent);
    	$completeContent = preg_replace("/elementor-column/", "ec", $completeContent);
    	$completeContent = preg_replace("/elementor-widget-container/", "ewcont", $completeContent);
    	$completeContent = preg_replace("/elementor-widget/", "ew", $completeContent);
    	$completeContent = preg_replace("/elementor-section/", "es", $completeContent);
    	$completeContent = preg_replace("/elementor-inner-column/", "eic", $completeContent);
    	$completeContent = preg_replace("/elementor-divider-separator/", "eds", $completeContent);
    	$completeContent = preg_replace("/elementor-inner/", "einn", $completeContent);
    	$completeContent = preg_replace("/elementor-heading-title/", "eht", $completeContent);
    	$completeContent = preg_replace("/elementor-spacer-inner/", "esi", $completeContent);
    	$completeContent = preg_replace("/elementor-background-overlay/", "ebgov", $completeContent);
    	$completeContent = preg_replace("/elementor-icon-list-items/", "eilts", $completeContent);
    	$completeContent = preg_replace("/elementor-icon-list-item/", "eilt", $completeContent);
    	$completeContent = preg_replace("/elementor-button/", "ebtn", $completeContent);
    	$completeContent = preg_replace("/elementor-reverse-mobile/", "erev", $completeContent);	
    	//Gravity
    	$completeContent = preg_replace("/gform_fields/", "gff", $completeContent);
    	//$completeContent = preg_replace("/elementor-/", "ele-", $completeContent);
    	$completeContent = preg_replace("/elementor-invisible/s", "", $completeContent);
    	$completeContent = preg_replace("/elementor-col/s", "ecol", $completeContent);
    	$completeContent = preg_replace("/elementor-/s", "ele-", $completeContent);

    	$completeContent = preg_replace("/ele-testimonial/", "etml", $completeContent);
		$completeContent = preg_replace("/etml-wrapper/", "etmlwr", $completeContent);
		$completeContent = preg_replace("/.ew-image .ele-image>a,/", "", $completeContent);
		//col_item
      $completeContent = preg_replace("/col_item/", "ci", $completeContent); 
      //col_wrap_fourth
      $completeContent = preg_replace("/col_wrap_fourth/", "cf", $completeContent);  
      //col-feat-grid 
      $completeContent = preg_replace("/col-feat-grid/", "cg", $completeContent);
      //deal_daywoo
      $completeContent = preg_replace("/deal_daywoo/", "dd", $completeContent);
      //categoriesbox
      $completeContent = preg_replace("/categoriesbox/", "cb", $completeContent);
      //e-tg-item
      $completeContent = preg_replace("/e-tg-item/", "ei", $completeContent);
      //button_action
      $completeContent = preg_replace("/button_action/", "ba", $completeContent);
		$completeContent = preg_replace("/ele-motion-effects-element-type-background/s", "ele-meetb", $completeContent);
		//CSS Optimization.
		$completeContent = preg_replace("/ele-share-buttons--skin-gradient/", "e-sbsg", $completeContent);
		$completeContent = preg_replace("/ele-share-buttons--skin-flat/", "e-sb-sf", $completeContent);
		$completeContent = preg_replace("/post__/", "pst_", $completeContent);
		$completeContent = preg_replace("/ele-post-navigation/", "e-pnvgn", $completeContent);
		$completeContent = preg_replace("/ele-pst_meta-data/", "e-p_m-d", $completeContent);
		$completeContent = preg_replace("/nav-menu/", "n-m", $completeContent);
		$completeContent = preg_replace("/ele-n-m--main/", "e-n-m-m", $completeContent);
		$completeContent = preg_replace("/post-navigation__prev--label/", "p-np-l", $completeContent);
		$completeContent = preg_replace("/ele-share-buttons--color-official/", "e-sb-cof", $completeContent);
		$completeContent = preg_replace("/ele-share-btn__text/", "e-s-b_te", $completeContent);
		$completeContent = preg_replace("/ele-share-btn__icon/", "e-s-bt_i", $completeContent);
		$completeContent = preg_replace("/ele-share-btn/", "e-se-bn", $completeContent);
		$completeContent = preg_replace("/ele-posts--skin-cards/", "e-p-scs", $completeContent);
		$completeContent = preg_replace("/ele-share-buttons--view-i/", "es-bv-i", $completeContent);
		$completeContent = preg_replace("/ele-share-buttons/", "e-s-bs", $completeContent);
		$completeContent = preg_replace("/ele-field-group/", "e-f-gp", $completeContent);
		$completeContent = preg_replace("/ele-field-textual/", "e-f-tl", $completeContent);
		$completeContent = preg_replace("/ele-field/", "e-fd", $completeContent);
		$completeContent = preg_replace("/ele-form-fields-wrapper/", "e-ff-w", $completeContent);
		$completeContent = preg_replace("/ele-grid/", "e-gr", $completeContent);
		$completeContent = preg_replace("/ele-n-m--dropdown/", "e-n-dn", $completeContent);
		$completeContent = preg_replace("/ele-posts--thumbnail-top/", "e-pt-t", $completeContent);
		$completeContent = preg_replace("/ele-sitemap-section/", "e-sm-s", $completeContent);
		$completeContent = preg_replace("/post-navigation__prev--title/", "p-n_p-t", $completeContent);
		$completeContent = preg_replace("/post-navigation__next--label/", "p-n_n-l", $completeContent);
		$completeContent = preg_replace("/-webkit-box-orient:(.*?);/", "", $completeContent);
		 $completeContent = preg_replace("/-webkit-box-pack:(.*?);/", "", $completeContent);
		$completeContent = preg_replace("/-webkit-justify-content:(.*?);/", "", $completeContent);
		$completeContent = preg_replace("/-webkit-align-items:(.*?);/", "", $completeContent);
		$completeContent = preg_replace("/-ms-flex-align:(.*?);/", "", $completeContent);
		$completeContent = preg_replace("/-ms-transform:(.*?);/", "", $completeContent);
		$completeContent = preg_replace("/-ms-flex-item-align:(.*?);/", "", $completeContent);
		$completeContent = preg_replace("/-ms-flex-wrap:(.*?);/", "", $completeContent);
        $completeContent = preg_replace("/-webkit-transform:(.*?);/", "", $completeContent);
        $completeContent = preg_replace("/-webkit-flex-grow:(.*?);/", "", $completeContent);
        $completeContent = preg_replace("/display:-webkit-box;/", "", $completeContent);
        $completeContent = preg_replace("/-webkit-border-radius:(.*?);/", "", $completeContent);
        $completeContent = preg_replace("/-webkit-transition:(.*?);/", "", $completeContent);
        $completeContent = preg_replace("/display:-webkit-flex;/", "", $completeContent);
        $completeContent = preg_replace("/display:-ms-flexbox;/", "", $completeContent);
		$completeContent = preg_replace("/-select-wrapper/s", "-sewr", $completeContent);
		$completeContent = preg_replace("/-shape-bottom/s", "-shb", $completeContent);
		$completeContent = preg_replace("/-shape-fill/s", "-shf", $completeContent);
		$completeContent = preg_replace("/-icon-box-title/s", "-icbtit", $completeContent);
		$completeContent = preg_replace("/ele-star-rating/s", "esr", $completeContent);
		$completeContent = preg_replace("/ec-wrap/s", "ewr", $completeContent);
		$completeContent = preg_replace("/ew-wrap/s", "ew-r", $completeContent);
		$completeContent = preg_replace("/ew-heading/s", "ehd", $completeContent);
	    $completeContent = preg_replace("/ele-view-framed/s", "e-vf", $completeContent);
	    //$completeContent = preg_replace("/-icon/s", "-i", $completeContent);
	    $completeContent = preg_replace("/ele-toggle/s", "e-tg", $completeContent);
	    $completeContent = preg_replace("/ewcont/s", "ect", $completeContent);
	    $completeContent = preg_replace("/ele-image/s", "eim", $completeContent);
        $completeContent = preg_replace("/ele-i-list-/s", "eil-", $completeContent);
        //$completeContent = preg_replace("/ee-/s", "ee", $completeContent);
        $completeContent = preg_replace("/amp-menu/s", "a-m", $completeContent);
        $completeContent = preg_replace("/amp-category/s", "ampcat", $completeContent);
        $completeContent = preg_replace("/loop-category/s", "lpct", $completeContent);
        $completeContent = preg_replace("/breadcrumb/s", "bdcr", $completeContent);
        $completeContent = preg_replace("/\.ele-([0-9]*)\s\.ee.ee([a-zA-Z0-9]*)\s>\s(\.epo|\.ect){padding:0px 0px 0px 0px}/s", "", $completeContent);
        $completeContent = preg_replace("/\.ele-([0-9]*)\s.ee.ee([a-zA-Z0-9]*)\.ec\s\.ewr{align-items:center}/s", ".ee.ee$2.ec .ewr{align-items:center}", $completeContent);
         $completeContent = preg_replace("/body:not\(.rtl\)\s\.ele-([0-9]*)\s\.ee.ee([a-zA-Z0-9]*)\s\.esr\si:not\(:last-of-type\){margin-right:[a-zA-Z0-9]*}/","", $completeContent);
         $completeContent = preg_replace("/.woocommerce amp-img,.w-pg amp-img{height:auto;max-width:100%}/",".woocommerce amp-img,.w-pg amp-img{height:auto;max-width:38%}", $completeContent);
         $completeContent = preg_replace("/ele-testimonial-wrapper/","ele-t-w", $completeContent);
         $completeContent = preg_replace("/ew-divider/","ew-dv", $completeContent);
         $completeContent = preg_replace("/ele-i-wrapper/","ele-i-w", $completeContent);
         $completeContent = preg_replace("/ele-view-default/","e-vd", $completeContent);
         $completeContent = preg_replace("/premium-button/","p-bt", $completeContent);
         $completeContent = preg_replace("/ew-premium-addon-button/","ew-p-a-b", $completeContent);
         $completeContent = preg_replace("/gform_wrapper/","g_w", $completeContent);
         $completeContent = preg_replace("/ginput_container/","g_c", $completeContent);
         $completeContent = preg_replace("/divider/","dd", $completeContent);
         $completeContent = preg_replace("/transform:rotate\(0deg\)(.*?)/","", $completeContent);
         $completeContent = preg_replace("/font-style:var\(--body_typography-(.*?)\);/","", $completeContent);
         $completeContent = preg_replace("/--body_typography-(.*?)/","", $completeContent);
         $completeContent = preg_replace("/gfield_label/","g_l", $completeContent);
         $completeContent = preg_replace("/text-i-wrapper/","t-i-w", $completeContent);
         $completeContent = preg_replace("/transition:background .3s,border .3s,border-radius .3s,box-shadow .3s/","", $completeContent);
         $completeContent = preg_replace("/ele-i-box-wrapper/","ele-i-b-w", $completeContent);
         $completeContent = preg_replace("/price-table/","p", $completeContent);
         $completeContent = preg_replace("/ele-position/","ele-po", $completeContent);
         $completeContent = preg_replace("/-boxed/","-bx", $completeContent);
         $completeContent = preg_replace("/box-content/","bc", $completeContent);
         $completeContent = preg_replace("/heading/","he", $completeContent);
         $completeContent = preg_replace("/es-height-default/","es-h-d", $completeContent);
         $completeContent = preg_replace("/e-animation/","e-an", $completeContent);
         $completeContent = preg_replace("/ele-p__/","ele-p_", $completeContent);
         $completeContent = preg_replace("/ele-p_features-list/","ele-p_f-l", $completeContent);
         $completeContent = preg_replace("/ele-p_feature-inner/","ele-p_f-i", $completeContent);
         $completeContent = preg_replace("/-webkit-box-sizing:border-box;/","", $completeContent);
         $completeContent = preg_replace("/-webkit-font-smoothing:antialiased;/","", $completeContent);
         $completeContent = preg_replace("/-moz-osx-font-smoothing:grayscale/","", $completeContent);
         $completeContent = preg_replace("/ele-vertical-align/","ele-v-a", $completeContent);
         $completeContent = preg_replace("/ele-repeater-item/","ele-r-i", $completeContent);
         $completeContent = preg_replace("/ele-size/","ele-s", $completeContent);
         $completeContent = preg_replace("/ele-top-section/","ele-t-s", $completeContent);
        // $completeContent = preg_replace("/testimonial/","tm", $completeContent);
         $completeContent = preg_replace("/size-default/","s-d", $completeContent);
         $completeContent = preg_replace("/top-column/","t-c", $completeContent);
         $completeContent = preg_replace("/-default/","df", $completeContent);
         $completeContent = preg_replace("/einn-section/","einn-s", $completeContent);
         $completeContent = preg_replace("/a-top/","a-t", $completeContent);
         $completeContent = preg_replace("/i-box/","i-b", $completeContent);
         $completeContent = preg_replace("/currency/","cy", $completeContent);
         $completeContent = preg_replace("/p_header/","p_h", $completeContent);
         $completeContent = preg_replace("/fractional-part/","f-p", $completeContent);
         $completeContent = preg_replace("/p_footer/","p_ft", $completeContent);
         $completeContent = preg_replace("/-an-grow/","-a-g", $completeContent);
         $completeContent = preg_replace("/po-top/","p-tp", $completeContent);
         $completeContent = preg_replace("/gapdf/","gd", $completeContent);
         $completeContent = preg_replace("/content-middle/","c-m", $completeContent);
         $completeContent = preg_replace("/ew-image/","ew-ig", $completeContent);
         $completeContent = preg_replace("/gap-no/","g-n", $completeContent);
         $completeContent = preg_replace("/_price/","_pr", $completeContent);
         $completeContent = preg_replace("/amp-post-title/","a-p-t", $completeContent);
         $completeContent = preg_replace("/ampforwp_contact_bar/","a_c_b", $completeContent);
         $completeContent = preg_replace("/ginput_complex/","gi_c", $completeContent);
         $completeContent = preg_replace("/etml-content/","el-c", $completeContent);
         $completeContent = preg_replace("/menu-main-menu/","m-m-m", $completeContent);
         $completeContent = preg_replace("/sub-menu/","s-m", $completeContent);
         $completeContent = preg_replace("/amp-search/","a-s", $completeContent);
         $completeContent = preg_replace("/s-submit/","s-s", $completeContent);
         $completeContent = preg_replace("/-wrapper/","-wr", $completeContent);
         $completeContent = preg_replace('/data-col="/','data-c="', $completeContent);
         $completeContent = preg_replace('/srch/','s', $completeContent);
         $completeContent = preg_replace('/ele-form/','ele-f', $completeContent);
         $completeContent = preg_replace('/screen-reader-text/','s-r-t', $completeContent);
         $completeContent = preg_replace('/p_button/','p_bt', $completeContent);
         $completeContent = preg_replace('/-text-editor/','-t-ed', $completeContent);
         $completeContent = preg_replace('/p_after-price/','p_a-p', $completeContent);
         $completeContent = preg_replace('/p_integer-part/','p_i-p', $completeContent);

         $re = '/data-id="([a-z0-9]+)"/m';
         preg_match_all($re, $completeContent, $matches, PREG_SET_ORDER, 0);
         foreach ($matches as $key => $value) {
           $replacer = $value[1];
             $replacwith =  substr($value[1], -3);
             $completeContent = preg_replace("/ee".$replacer."/", "e".$replacwith, $completeContent);
         }

		//Code to remove unused CSS starts here
    	$swift_cntnWrp_remover = apply_filters("amp_pb_comp_swift_unused_remover", false);
    	if($swift_cntnWrp_remover==true){
	    	$completeContent = preg_replace("/\.cntn-wrp{\s*font-size\s*:\s*18px;\s*color\s*:\s*#000;line-height:\s*1\.7\s*;}/s", "", $completeContent);
	    }
	    //Code to remove unused CSS ends here
	    //Code to remove Header and Footer starts here
	    if(class_exists("\ElementorPro\Plugin")){
	    	
	    	if( pagebuilder_for_amp_utils::get_setting('elem-remove_header_footer' ) ) {
	    		$completeContent = preg_replace("/<header(.*?)class=\"header[-|0-9]* h_m h_m_1\">(.*?)<\/header>/s", "", $completeContent);
	    		$completeContent = preg_replace("/<footer(.*?)class=\"footer\">(.*?)<\/footer>/si", "", $completeContent);
	    	}
            $completeContent = preg_replace_callback('/<amp-img\slightbox=\"(.*?)\"(.*?)><amp-img\sfallback\slightbox=\"(.*?)\"(.*?)><\/amp-img><\/amp-img>/', function($m){
                $amp_img_lightbox = '<amp-img lightbox="'.$m[1].'"'.$m[2].'><amp-img fallback '.$m[4].'></amp-img></amp-img>';
                return $amp_img_lightbox;
            }, $completeContent);
		}
		$completeContent = apply_filters("amp_pc_elementor_css_sorting", $completeContent);
	    //Code to remove Header and Footer ends here
    	return $completeContent;
    }

	public function amp_elementor_custom_styles(){
		global $post;
		global $amp_elemetor_custom_css;
		if ( $post ){
			$postID = $post->ID;
	         if ( function_exists('ampforwp_is_front_page') && ampforwp_is_front_page() ) {
	            $postID = ampforwp_get_frontpage_id();
	        }
	        $this->postID = $postID;
		}
		if(class_exists('\Elementor\Plugin') && !( \Elementor\Plugin::$instance->db->is_built_with_elementor($postID) ) ){
			return ;
		}
		$allCss = '';

		$srcs = array();
		$min_suffix =  '.min';
		$direction_suffix = is_rtl() ? '-rtl' : '';
		$frontend_file_name = 'frontend' . $direction_suffix . $min_suffix . '.css';
		$srcs[] = ELEMENTOR_ASSETS_URL . 'css/' . $frontend_file_name;
        $srcs[] =  plugin_dir_url(DIR).'elementor/assets/lib/font-awesome/css/all.min.css';
        $srcs[] =  plugin_dir_url(DIR).'elementor/assets/lib/font-awesome/css/brands.min.css';
		$srcs[] =  plugin_dir_url(DIR).'elementor/assets/lib/font-awesome/css/solid.min.css';
        $srcs[] =  plugin_dir_url(DIR).'elementor/assets/lib/font-awesome/css/regular.min.css';

		if(is_plugin_active('amp-woocommerce-pro/amp-woocommerce.php')){
		$srcs[] =  plugin_dir_url(DIR).'amp-pagebuilder-compatibility/widgets/pro/styles.css';
		}
		if(function_exists('wp_upload_dir')){
			$uploadUrl = wp_upload_dir()['baseurl'];
			$srcs[] = $uploadUrl."/elementor/css/global.css";
			$srcs[] = $uploadUrl."/elementor/css/post-". $this->postID.".css";
			if(class_exists('\ElementorPro\Plugin')){
			    $plugins_url = plugins_url();
				$srcs[] =  $plugins_url.'/elementor-pro/assets/css/frontend.min.css';

				$frontend_file_url = '';
				$frontend_file_name = 'frontend' . $direction_suffix . $min_suffix . '.css';
				$has_custom_file = \Elementor\Core\Responsive\Responsive::has_custom_breakpoints();
				if ( $has_custom_file ) {
					$frontend_file = new Elementor\Core\Responsive\Files\Frontend( 'custom-pro-' . $frontend_file_name, ELEMENTOR_PRO_ASSETS_PATH . 'css/templates/' . $frontend_file_name );
					$time = $frontend_file->get_meta( 'time' );
					if ( ! $time ) {
						$frontend_file->update();
					}
					$frontend_file_url = $frontend_file->get_url();
				} else {
					$frontend_file_url = ELEMENTOR_PRO_ASSETS_URL . 'css/' . $frontend_file_name;
				}
				if($frontend_file_url){
					$srcs[] = $frontend_file_url;
				}

		    }
		}
		$srcs[] = ELEMENTOR_ASSETS_URL. '/lib/eicons/css/elementor-icons.css' ;
		 //include rehub theme css for woocommerce grid module.
         $theme = wp_get_theme(); // gets the current theme
	           if ( 'Rehub theme' == $theme->name ) {
	           
	              if(is_child_theme()){
	                  $srcs[] = get_template_directory_uri() . '/style.css';
	              }
	                  $srcs[] = get_stylesheet_directory_uri() . '/style.css';
	                 
	           }
		//Supported plugin css
		$plugin_css = $this->supported_plugin_css();
		if($plugin_css){
			$srcs = array_merge($srcs, $plugin_css);
		}
		$update_css = pagebuilder_for_amp_utils::get_setting_data('elementorCssKeys');
		if($update_css!='' && strpos($update_css, ',')!==false){
			$csslinks = explode(",", $update_css);
			$csslinks = array_filter(array_map('trim', $csslinks));
			$srcs = array_merge($srcs, $csslinks);
		}elseif(filter_var($update_css, FILTER_VALIDATE_URL)){
			$srcs = array_merge($srcs, array($update_css));
		}
		if(count($srcs)>0){
			$srcs = array_unique($srcs);
		}

		foreach ($srcs as $key => $urlValue) {
			$cssData = $this->ampforwp_remote_content($urlValue);
			$cssData = preg_replace("/\/\*(.*?)\*\//si", "", $cssData);
			$allCss .= preg_replace_callback('/url[(](.*?)[)]/', function($matches)use($urlValue){
                    $matches[1] = str_replace(array('"', "'"), array('', ''), $matches[1]);
                        if(!wp_http_validate_url($matches[1]) && strpos($matches[1],"data:")===false){
                            $urlExploded = explode("/", $urlValue);
                            $parentUrl = str_replace(end($urlExploded), "", $urlValue);
                            return 'url('.$parentUrl.$matches[1].")"; 
                        }else{
                            return $matches[0];
                        }
                    }, $cssData);
		}
		if(!empty(pagebuilder_for_amp_utils::get_setting_data('elementorCss-custom') ) ) {
            $allCss .= pagebuilder_for_amp_utils::get_setting_data('elementorCss-custom');
        }
		$allCss .= $this->supported_plugin_compatible_css();
		if( function_exists('wp_get_custom_css') ){
			$allCss .= wp_get_custom_css();
		}

	// for inline embedded css.
		$postID = $post->ID;
	        if ( function_exists('ampforwp_is_front_page') && ampforwp_is_front_page() ) {
	            $postID = ampforwp_get_frontpage_id();
	        }
		$fontsList = get_option( '_elementor_global_css');
		$postFontsList = get_post_meta($postID, '_elementor_css');

        $status = $postFontsList[0]['status'];
		$inlineCss = $postFontsList[0]['css'];
		if($status){
			$allCss .= $inlineCss;

		}
		$allCss = preg_replace("/\/\*(.*?)\*\//si", "", $allCss);
		 $allCss = str_replace(array(" img", "!important"), array(" amp-img", ""), $allCss);
         if ( in_array( 'content-egg/content-egg.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) )   )  || wp_get_theme( get_template() )->get( 'Name' ) == 'Rehub theme' ) {
         $allCss = '.wpsm_pros ul li span {top:1px;position: relative;}.wpsm_cons ul li span {top: 5px;position: relative;}.post ul li {list-style: disc outside none;}.wpsm_cons ul li:before {content: "\f056";color:#f24f4f;font: normal normal normal 14px/1 FontAwesome;font-size: 22px;}.artl-cnt ul li:before {display: inline-block;width: 0px;height: 0px;top: 9px;left: -23px;}.artl-cnt ul li {padding-left: 12px;list-style-type: disc;}';
        }
		$allCss .= 'header .cntr {max-width: 1100px;margin: 0 auto;width:100%;padding:0px 20px;}.elementor-testimonial-wrapper .elementor-testimonial-meta .elementor-testimonial-image amp-img { max-width: 60px; }amp-img{margin:0 auto;}
.footer{margin:0;}i.eicon-chevron-left {display: none;}i.eicon-chevron-right {display: none;}.ele-posts-container .ele-pst_thumbnail amp-img {max-width: 100%;}.cntr{max-width: 1100px;margin: 0 auto;width: 100%;padding: 0px 20px;} .elementor .carousel-preview amp-img{ height: 40px; width: 60px;position: relative;} .woocommerce .products li.product a.woocommerce-LoopProduct-link amp-img {margin: 0 auto;}';
		if(is_array($amp_elemetor_custom_css)){
			foreach ($amp_elemetor_custom_css as $key => $cssArray) {
				if(is_array($cssArray)){
					foreach ($cssArray as $key => $css) {
						$allCss .= $css;
					}
				}else{
					$allCss .= $cssArray;
				}
				
			}
		}
		echo $allCss;
		
	}

	public function ampforwp_body_class_elementor($classes){
		if(class_exists('\Elementor\Frontend')){
			$elemFrontEndobj = new \Elementor\Frontend();
			$classes = array_merge($classes, $elemFrontEndobj->body_class());
		}
		return $classes;
	}

	public function ampforwp_remote_content($src){
		if($src){
			$arg = array( "sslverify" => false, "timeout" => 60 ) ;
			$response = wp_remote_get( $src, $arg );
	        if ( wp_remote_retrieve_response_code($response) == 200 && is_array( $response ) ) {
	          $header = wp_remote_retrieve_headers($response); // array of http header lines
	          $contentData =  wp_remote_retrieve_body($response); // use the content
	          return $contentData;
	        }else{
				$contentData = file_get_contents( $src );
				if(! $contentData ){
					$data = str_replace(get_site_url(), '', $src);//content_url()
					$data = getcwd().$data;
					if(file_exists($data)){
						$contentData = file_get_contents($data);
					}
				}
				return $contentData;
			}

		}
        return '';
	}


	function elem_amp_fonts(){
		echo "<link rel='stylesheet' id='font-awesome-css'  href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' type='text/css' media='all' />";
		$this->amp_fonts_elementor();
	}

	function amp_fonts_elementor(){
		global $post;
		$fontsList = get_option( '_elementor_global_css');
		$postFontsList = get_post_meta($post->ID, '_elementor_css');
		$listPostFonts = array();
		foreach ($postFontsList as $key => $postFonts) {
			$listPostFonts = array_merge( $listPostFonts, $postFonts['fonts']);
		}
		$fonts =  $listPostFonts;
		if($fontsList && isset($fontsList['fonts'])){
			$fonts = array_merge($fontsList['fonts'], $fonts);
		}
		$google_fonts = array();
		if($fonts){
			foreach ($fonts as $key => $font) {
				$font_type = \Elementor\Fonts::get_font_type( $font );
					switch ( $font_type ) {
						case \Elementor\Fonts::GOOGLE:
							$google_fonts['google'][] = $font;
							break;
						case \Elementor\Fonts::EARLYACCESS:
							$google_fonts['early'][] = $font;
							break;
						}
			}
		}
		$google_fonts_index = 1;
		if ( ! empty( $google_fonts['google'] ) ) {
				$google_fonts_index++;
				foreach ( $google_fonts['google'] as &$font ) {
					$font = str_replace( ' ', '+', $font ) . ':100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic';
				}
				$fonts_url = sprintf( 'https://fonts.googleapis.com/css?family=%s', implode( rawurlencode( '|' ), $google_fonts['google'] ) );
				$subsets = [
					'ru_RU' => 'cyrillic',
					'bg_BG' => 'cyrillic',
					'he_IL' => 'hebrew',
					'el' => 'greek',
					'vi' => 'vietnamese',
					'uk' => 'cyrillic',
					'cs_CZ' => 'latin-ext',
					'ro_RO' => 'latin-ext',
					'pl_PL' => 'latin-ext',
				];
				$locale = get_locale();
				if ( isset( $subsets[ $locale ] ) ) {
					$fonts_url .= '&subset=' . $subsets[ $locale ];
				}
				echo "<link rel='stylesheet' id='google-fonts'  href='$fonts_url' type='text/css' media='all' />";
			}
			if ( ! empty( $google_fonts['early'] ) ) {
				foreach ( $google_fonts['early'] as $current_font ) {
					$google_fonts_index++;
					
					$fonts_url = sprintf( 'https://fonts.googleapis.com/earlyaccess/%s.css', strtolower( str_replace( ' ', '', $current_font ) ) );
					
					echo "<link rel='stylesheet' id='google-fonts'  href='$fonts_url' type='text/css' media='all' />";
				}
			}
		
	}
	function supported_plugin_css(){
		$cssUrl = array();
		if(class_exists('Jet_Blog') && function_exists('jet_blog') ){
			$cssUrl[] = jet_blog()->plugin_url( 'assets/css/jet-blog.css' );
		}
		if(class_exists('jet_elements') && function_exists('jet_elements') ){
			$cssUrl[] = jet_elements()->plugin_url( 'assets/css/jet-elements.css' );
		}
		if(class_exists("\UltimateElementor\Classes\UAEL_Config")){
			$allCss = \UltimateElementor\Classes\UAEL_Config::get_widget_style();
			if($allCss){
				foreach ($allCss as $key => $css) {
					$cssUrl[] = UAEL_URL.$css['path'];
				}
			}
		}
		return $cssUrl;
	}
	function supported_plugin_compatible_css(){
		$css = '';
		if(class_exists('Jet_Blog') && function_exists('jet_blog') ){
			$css = '.jet-smart-listing__post{opacity:1;}';
		}
		if(class_exists("\UltimateElementor\Classes\UAEL_Config")){
			$css .= '.jet-smart-listing__featured{opacity:1;}';
		}
		return $css;
	}

	function ampforwp_pdf_embedder_compatibility(){
		if( ( function_exists('ampforwp_is_amp_endpoint')  && ampforwp_is_amp_endpoint() ) || ( function_exists('is_amp_endpoint')  && is_amp_endpoint() ) ) {
	            remove_shortcode('pdf-embedder');
	            add_shortcode('pdf-embedder',[$this, 'ampforwp_pdfemb_shortcode_display_pdf'] );
	    }
	}

	function ampforwp_pdfemb_shortcode_display_pdf($atts, $content=null) {
		$atts = apply_filters('pdfemb_filter_shortcode_attrs', $atts);

		if (!isset($atts['url'])) {
			return '<b>PDF Embedder requires a url attribute</b>';
		}
		$url = $atts['url'];
		add_filter('amp_post_template_data', array($this, 'amp_elementor_amp_google_document_embed_scripts'), 20);

		return '<amp-google-document-embed  src="'.$url.'" width="8.5"  height="11"
	      layout="responsive"></amp-google-document-embed>';
	}

	function amp_elementor_amp_google_document_embed_scripts($data){
		$data['amp_component_scripts']['amp-google-document-embed'] = 'https://cdn.ampproject.org/v0/amp-google-document-embed-0.1.js';
		return $data;
	}

	function ampforwp_blacklist_sanitizer($data){
        require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'/includes/class-amp-divi-blacklist.php';
        unset($data['AMP_Blacklist_Sanitizer']);
        unset($data['AMPFORWP_Blacklist_Sanitizer']);
        $data[ 'AMPFORWP_DIVI_Blacklist' ] = array();
        return $data;
    }
    function recaptcha_validation( $record, $ajax_handler ){
    	$secretkey = pagebuilder_for_amp_utils::get_setting('elem-recaptcha-secretkey');
    	$fields = $record->get_field( [
			'type' => 'recaptcha',
		] );

		if ( empty( $fields ) ) {
			return;
		}

		$field = current( $fields );

		if ( empty( $_POST['g-recaptcha-response'] ) ) {
			$ajax_handler->add_error( $field['id'], __( 'The Captcha field cannot be blank. Please enter a value.', 'elementor-pro' ) );

			return;
		}

		$recaptcha_errors = [
			'missing-input-secret' => __( 'The secret parameter is missing.', 'elementor-pro' ),
			'invalid-input-secret' => __( 'The secret parameter is invalid or malformed.', 'elementor-pro' ),
			'missing-input-response' => __( 'The response parameter is missing.', 'elementor-pro' ),
			'invalid-input-response' => __( 'The response parameter is invalid or malformed.', 'elementor-pro' ),
		];

		$recaptcha_response = $_POST['g-recaptcha-response'];
		$recaptcha_secret = $secretkey;
		$client_ip = Utils::get_client_ip();

		$request = [
			'body' => [
				'secret' => $recaptcha_secret,
				'response' => $recaptcha_response,
				'remoteip' => $client_ip,
			],
		];

		$response = wp_remote_post( 'https://www.google.com/recaptcha/api/siteverify', $request );

		$response_code = wp_remote_retrieve_response_code( $response );

		if ( 200 !== (int) $response_code ) {
			/* translators: %d: Response code. */
			$ajax_handler->add_error( $field['id'], sprintf( __( 'Can not connect to the reCAPTCHA server (%d).', 'elementor-pro' ), $response_code ) );

			return;
		}

		$body = wp_remote_retrieve_body( $response );

		$result = json_decode( $body, true );

		if ( ! $result['success'] ) {
			$message = __( 'Invalid Form.', 'elementor-pro' );

			$result_errors = array_flip( $result['error-codes'] );

			foreach ( $recaptcha_errors as $error_key => $error_desc ) {
				if ( isset( $result_errors[ $error_key ] ) ) {
					$message = $recaptcha_errors[ $error_key ];
					break;
				}
			}
			$ajax_handler->add_error( $field['id'], $message );
		}

		// If success - remove the field form list (don't send it in emails and etc )
		$record->remove_field( $field['id'] );
    }
	public function elementor_amp_cform_submission(){
		require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'/widgets/amp-form-submit.php' );

	}

public function amp_elementor_ajax_comment(){
  global $redux_builder_amp;
  header("access-control-allow-credentials:true");
  header("access-control-allow-headers:Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token");
  header("Access-Control-Allow-Origin:".$_SERVER['HTTP_ORIGIN']);
  $siteUrl = parse_url(  get_site_url() );
  header("AMP-Access-Control-Allow-Source-Origin:".$siteUrl['scheme'] . '://' . $siteUrl['host']);
  header("access-control-expose-headers:AMP-Access-Control-Allow-Source-Origin");
  header("Content-Type:application/json;charset=utf-8");
  $comment = wp_handle_comment_submission( wp_unslash( $_POST ) );
  $text_data = 'Thank you for submitting comment, we will review it and will get back to you.';
  if ( is_wp_error( $comment ) ) {
    $error_data = intval( $comment->get_error_data() );
    if ( ! empty( $error_data ) ) {
      $comment_html = $comment->get_error_message();
      $comment_html = str_replace("&#8217;","'",$comment_html);
      $comment_html = str_replace(array('<strong>','</strong>'),array('',''),$comment_html);
      $comment_status = array('response' => $comment_html );
      header('HTTP/1.1 500 FORBIDDEN');
      echo json_encode($comment_status);
      die;
      // wp_die( '<p>' . $comment->get_error_message() . '</p>', __( 'Comment Submission Failure' ), array( 'response' => $error_data, 'back_link' => true ) );
    } else {
      wp_die( 'Unknown error' );
    }
  }

  $user = wp_get_current_user();
  do_action('set_comment_cookies', $comment, $user);
 
  $comment_depth = 1;
  $comment_parent = $comment->comment_parent;
  while( $comment_parent ){
    $comment_depth++;
    $parent_comment = get_comment( $comment_parent );
    $comment_parent = $parent_comment->comment_parent;
  }
 
  $GLOBALS['comment'] = $comment;
  $GLOBALS['comment_depth'] = $comment_depth;
  $comment_html = $text_data;
  $comment_status = array('response' => $comment_html );


  echo json_encode($comment_status);
  //echo $comment_html;
  die;
}


	public function any_content_sanitizer($content){
		$sanitizer_obj = new AMPFORWP_Content( $content,
								array(), 
								apply_filters( 'amp_content_sanitizers', 
									array( 'AMP_Img_Sanitizer' => array(), 
										'AMP_Blacklist_Sanitizer' => array(),
										'AMP_Style_Sanitizer' => array(), 
										'AMP_Video_Sanitizer' => array(),
				 						'AMP_Audio_Sanitizer' => array(),
				 						'AMP_Iframe_Sanitizer' => array(
											 'add_placeholder' => true,
										 ),
									) 
								) 
							);
		$this->sanitizer_script = $sanitizer_obj->get_amp_scripts();
		$this->sanitizer_style = $sanitizer_obj->get_amp_styles();
		 return $sanitizer_obj->get_amp_content();
	}
}



/**
* Admin section portal Access
**/
add_action('plugins_loaded', 'pagebuilder_for_amp_elementor_option');
function pagebuilder_for_amp_elementor_option(){
	if(is_admin()){
		new pagebuilder_for_amp_elementor_Admin();
	}else{
		// Instantiate Elementor_For_Amp.
	}
	new Elementor_For_Amp();
}
Class pagebuilder_for_amp_elementor_Admin{
	function __construct(){
		add_filter( 'redux/options/redux_builder_amp/sections', array($this, 'add_options_for_elementor'),7,1 );
	}
	public static function get_admin_options($section = array()){
		$obj = new self();
		//print_r($obj);die;
		$section = $obj->add_options_for_elementor($section);
		return $section;
	}
	function add_options_for_elementor($sections){
		$desc = '';
		if(!class_exists('\Elementor\Plugin')){
			$desc = 'Enable/Activate Elementor plugin';
		}
		$accordionArray = array();
		$sectionskey = 0;
		foreach ($sections as $sectionskey => $sectionsData) {
			if($sectionsData['id']=='amp-content-builder' &&  count($sectionsData['fields'])>0 ){
				foreach ($sectionsData['fields'] as $fieldkey => $fieldvalue) {
					if($fieldvalue['id'] == 'ampforwp-elementor-pb-for-amp-accor'){
                    	$accordionArray = $sections[$sectionskey]['fields'][$fieldkey];
                    	 unset($sections[$sectionskey]['fields'][$fieldkey]);
                    }
                    if($fieldvalue['id'] == 'ampforwp-elementor-pb-for-amp'){
                        unset($sections[$sectionskey]['fields'][$fieldkey]);
                    }
				}
				break;
			}
		}
		$sections[$sectionskey]['fields'][] = $accordionArray;
		$sections[$sectionskey]['fields'][] = array(
				               'id'       => 'pagebuilder-for-amp-elementor-support',
				               'type'     => 'switch',
				               'title'    => esc_html__('AMP Elementor Compatibility ','accelerated-mobile-pages'),
				               'tooltip-subtitle' => esc_html__('Enable or Disable the Elementor for AMP', 'accelerated-mobile-pages'),
				               'desc'	  => $desc,
				               'section_id' => 'amp-content-builder',
				               'default'  => false
				            );
		foreach ($this->amp_elementor_fields() as $key => $value) {
        	$sections[$sectionskey]['fields'][] = $value;
        }

		return $sections;

	}

	public function amp_elementor_fields(){
        $contents[] = array(
                        'id'       => 'elementorCssKeys',
                        'type'     => 'textarea',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Enter css url', 'amp-pagebuilder-compatibility'),
                        'subtitle'  => esc_html__('', 'amp-pagebuilder-compatibility'),
                        'default'  => '',
                        'desc'      => esc_html__( 'Add your css url in comma saperated', 'amp-pagebuilder-compatibility' ),
                       // 'required'=> array(array('pagebuilder-for-amp-wpbakery-support','==', 1)),
                         'section_id' => 'amp-content-builder',

                    );
        $contents[] = array(
                        'id'       => 'elementorCss-custom',
                        'type'     => 'textarea',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Enter custom css', 'amp-pagebuilder-compatibility'),
                        'subtitle'  => esc_html__('', 'amp-pagebuilder-compatibility'),
                        'default'  => '',
                        'desc'      => esc_html__( 'Add your custom css code', 'amp-pagebuilder-compatibility' ),
                         'section_id' => 'amp-content-builder',
                    );
        $contents[] = array(
                        'id'       => 'elem-recaptcha-sitekey',
                        'type'     => 'text',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Enter reCAPTCHA (v3) site key', 'amp-pagebuilder-compatibility'),
                        'subtitle'  => esc_html__('', 'amp-pagebuilder-compatibility'),
                        'default'  => '',
                        'desc'      => esc_html__( 'Recaptcha site key https://www.google.com/recaptcha', 'amp-pagebuilder-compatibility' ),
                         'section_id' => 'amp-content-builder',
                    );
        $contents[] = array(
                        'id'       => 'elem-recaptcha-secretkey',
                        'type'     => 'text',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Enter reCAPTCHA (v3) secret key', 'amp-pagebuilder-compatibility'),
                        'subtitle'  => esc_html__('', 'amp-pagebuilder-compatibility'),
                        'default'  => '',
                        'desc'      => esc_html__( 'Recaptcha site key https://www.google.com/recaptcha', 'amp-pagebuilder-compatibility' ),
                         'section_id' => 'amp-content-builder',
                    );
        $contents[] = array(
                        'id'       => 'elem-remove_header_footer',
                        'type'     => 'switch',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Remove Header Footer', 'amp-pagebuilder-compatibility'),
                        'subtitle'  => esc_html__('', 'amp-pagebuilder-compatibility'),
                        'default'  => '',
                        'desc'      => esc_html__( 'Tick if you want to remove default header & footer', 'amp-pagebuilder-compatibility' ),
                         'section_id' => 'amp-content-builder',
                    );
        return $contents;
    }
}
//remove lazyload on woocommerce grid module of ruhub theme.
add_filter('rh_lazy_load', 'lazyLoad');
function lazyLoad($lazy = false){
$getAMPurl = filter_input(INPUT_SERVER, 'REQUEST_URI');

$AmpUrl  = explode('/', $getAMPurl);
  //print_r($test);die;


if(in_array('amp', $AmpUrl) || in_array('?amp', $AmpUrl)){
   
     $lazy = false;
}
 return $lazy;
}

add_action('pre_amp_render_post','amp_pb_rehub_pr_cs');
function amp_pb_rehub_pr_cs(){
if ( in_array( 'content-egg/content-egg.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) )   )  || wp_get_theme( get_template() )->get( 'Name' ) == 'Rehub theme' ) {
 add_action('amp_post_template_css','amp_pb_rehub_css',999);
}
}
function amp_pb_rehub_css(){
    echo '.wpsm_pros ul li:before {content: "\f058";font: normal normal normal 14px/1 FontAwesome;color: #58c649;font-size: 22px;font-weight: 900;left: -35px;
position: relative;top: 5px;}.wpsm_pros ul li span {top:1px;position:relative;}'; 
}