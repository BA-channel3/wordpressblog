<?php
if ( ! defined( 'ABSPATH' ) ) exit;
//global $amp_diviCurrentStyleGlobal;
class AMP_PC_Divi_Pagebuidler {

    public function __construct()
    {
        $this->load_dependencies();
        //$this->define_public_hooks();
    }
    
    private function load_dependencies(){
        if(pagebuilder_for_amp_utils::get_setting('pagebuilder-for-amp-divi-support') ){
            add_filter('amp_post_template_css', [$this,'amp_divi_custom_styles'],11);
            add_filter('ampforwp_body_class', [$this,'ampforwp_body_class_divi'],11);
            //font load 
            add_filter('amp_post_template_css', [$this, 'amp_pbc_load_fonts'],11);
            add_action('amp_post_template_head', [$this, 'amp_divi_pagebuilder_font_link']);
            add_filter('ampforwp_pagebuilder_status_modify', [$this, 'pagebuilder_status_reset_divi'], 10, 2);
            add_action( 'et_builder_ready', [$this,'amp_divi_pagebuidler_override'] );
            add_filter('amp_content_sanitizers',[$this, 'ampforwp_gravity_blacklist_sanitizer'], 99);
            
            require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'/parser/index.php';
        }
    }
    public static function load_ajax_calls(){
        add_action('wp_ajax_divi_contact_form_submission',['AMP_PC_Divi_Pagebuidler','divi_contact_form_submission']);
        add_action('wp_ajax_nopriv_divi_contact_form_submission',['AMP_PC_Divi_Pagebuidler','divi_contact_form_submission']);
        add_action( 'wp_ajax_amp_divi_optin_newsletter_submit', array('AMP_PC_Divi_Pagebuidler', 'amp_divi_optin_newsletter_submit') );
        add_action( 'wp_ajax_nopriv_amp_divi_optin_newsletter_submit', array('AMP_PC_Divi_Pagebuidler', 'amp_divi_optin_newsletter_submit') );
    }
    public static function amp_divi_optin_newsletter_submit() {
        require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'/includes/divi-newsletter-form-submission.php';
    }
    public static function divi_contact_form_submission(){
        require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'/includes/divi-form-submission.php';
    }
    function ampforwp_gravity_blacklist_sanitizer($data){
     if(function_exists('ampforwp_is_front_page') && ampforwp_is_front_page()){
            require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'/includes/class-amp-divi-blacklist.php';
            unset($data['AMP_Blacklist_Sanitizer']);
            unset($data['AMPFORWP_Blacklist_Sanitizer']);
            $data[ 'AMPFORWP_DIVI_Blacklist' ] = array();

     }else{
            require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'/includes/class-amp-divi-blacklist.php';
            unset($data['AMP_Blacklist_Sanitizer']);
            unset($data['AMPFORWP_Blacklist_Sanitizer']);
            $data[ 'AMPFORWP_DIVI_Blacklist' ] = array();
        }
        return $data;
    }

    function pagebuilder_status_reset_divi($response, $postId ){
        if ( 'on' === get_post_meta( $postId, '_et_pb_use_builder', true ) ) {
            //$response = true;
        }
        return $response;
    }
    public static function classesReplacements($completeContent){
        $completeContent = preg_replace("/wpb_animate_when_almost_visible/", "", $completeContent);
        $completeContent = preg_replace("/et_animated\s/", "", $completeContent);
        $completeContent = preg_replace("/et-waypoint/", "", $completeContent);
        //
        $completeContent = preg_replace("/et_pb_module_/", "em_", $completeContent);
        $completeContent = preg_replace("/et_pb_module/", "em", $completeContent);

        $completeContent = preg_replace("/et_pb_column_/", "ec_", $completeContent);
        $completeContent = preg_replace("/et_pb_column/", "ec", $completeContent);

        $completeContent = preg_replace("/et_pb_gutters/", "egs", $completeContent);
        $completeContent = preg_replace("/et_pb_gutter/", "eg_", $completeContent);

        $completeContent = preg_replace("/et_pb_blurb_/", "eb_", $completeContent);
        $completeContent = preg_replace("/et_pb_blurb/", "eb", $completeContent);
        
        $completeContent = preg_replace("/et_pb_section_/", "es_", $completeContent);
        $completeContent = preg_replace("/et_pb_section/", "es", $completeContent);

        $completeContent = preg_replace("/et_pb_row_/", "er_", $completeContent);
        $completeContent = preg_replace("/et_pb_row/", "er", $completeContent);
        
        $completeContent = preg_replace("/et_pb_preload/", "", $completeContent);
        $completeContent = preg_replace("/data-columns>/", ">", $completeContent);
        $completeContent = preg_replace("/et_pb_/", "e_", $completeContent);
        $completeContent = preg_replace("/em_header/", "em_hd", $completeContent);
        $completeContent = preg_replace("/e_toggle_title/", "e_tg_t", $completeContent);
        $completeContent = preg_replace("/_toggle/", "_tg", $completeContent);
        $completeContent = preg_replace("/_fullwidth_header/", "fh", $completeContent);
        $completeContent = preg_replace("/_testimonial_author/", "tauth", $completeContent);
        $completeContent = preg_replace("/eb_content/", "ecnt", $completeContent);
        $completeContent = preg_replace("/eb_container/", "econt", $completeContent);
        $completeContent = preg_replace("/_text_align_left/", "txtal", $completeContent);
        $completeContent = preg_replace("/_bg_layout_dark/", "bglaydark", $completeContent);
        $completeContent = preg_replace("/e_contact_field/", "ecfield", $completeContent);
        $completeContent = preg_replace("/e_main_blurb_image/", "embimg", $completeContent);
        $completeContent = preg_replace("/e_image_wrap/", "eimgwrap", $completeContent);
        $completeContent = preg_replace("/e_number_counter/", "enumcounter", $completeContent);
        $completeContent = preg_replace("/e_button/", "ebtn", $completeContent);
        $completeContent = preg_replace("/e_contact/", "ecntct", $completeContent);
        $completeContent = preg_replace("/e_divider_position_center/", "edpcen", $completeContent);

        $completeContent = preg_replace("/e_image_/", "eim_", $completeContent);
        $completeContent = preg_replace("/e_image/", "eim", $completeContent);
        $completeContent = preg_replace("/e_text_/", "etx_", $completeContent);
        $completeContent = preg_replace("/e_text/", "etx", $completeContent);
        $completeContent = preg_replace("/menu-item/", "mit", $completeContent);
        $completeContent = preg_replace("/page-container/", "p-c", $completeContent);
        $completeContent = preg_replace("/_wrapper/", "wpr", $completeContent);
        $completeContent = preg_replace("/e_equal_columns/", "eecl", $completeContent);
       if(preg_match('/<div class="ebtn(.*?)e_bg_layout_light(.*?)>/', $completeContent)){
        $completeContent = preg_replace('/<div class="ebtn(.*?)e_bg_layout_light(.*?)>/', '<div class="">', $completeContent);
        }
        $completeContent = preg_replace("/@(-moz-|-webkit-|-ms-)*keyframes\s\w+{(\d%{(.*?)}\d+%{(.*?))+}}/", "", $completeContent);
        $completeContent = preg_replace("/@(-moz-|-webkit-|-ms-)*keyframes\s\w+{(.*?)}{2,}/", "", $completeContent);
        $completeContent = preg_replace("/\.cntn-wrp p,/s", "", $completeContent);

        $completeContent = preg_replace("/.e_slide:first-child .e_slideim amp-img{opacity:0}/", ".e_slide:first-child .e_slideim amp-img{opacity:1}", $completeContent);
        $completeContent = preg_replace("/.e_media_alignment_center .e_slideim{top:50%;bottom:auto}/", ".e_media_alignment_center .e_slideim{top:0;bottom:auto}", $completeContent);
        $completeContent = preg_replace("/.es_7{border-radius:0 0 0 16vw;overflow:hidden}/", ".es_7{border-radius:0 0 0 16vw;overflow:unset}", $completeContent);
//divi CSS optimization.
    $completeContent = preg_replace("/e_slide_description/", "e_sd", $completeContent);
    $completeContent = preg_replace("/slider_/", "sl_", $completeContent);
        
    $completeContent = preg_replace("/e_newsletter/", "e_nsl", $completeContent);
    $completeContent = preg_replace("/e_fullwidth/", "e_fw", $completeContent);
    $completeContent = preg_replace("/e_promo_button/", "e_pbtn", $completeContent);
    $completeContent = preg_replace("/e_promo_description/", "e_pdi", $completeContent);
    $completeContent = preg_replace("/e_media_alignment_center/", "e_alimnt", $completeContent);
    $completeContent = preg_replace("/e_css_mix_blend_mode_passthrough/", "e_cssmix", $completeContent);
    $completeContent = preg_replace("/et_section_regular/", "e_sere", $completeContent);
    $completeContent = preg_replace("/e_animation_to/", "e_anit", $completeContent);
    $completeContent = preg_replace("/e_with_background/", "e_wibd", $completeContent);
    $completeContent = preg_replace("/e_bg_layout_light/", "e_bg_ll", $completeContent);
    $completeContent = preg_replace("/box-shadow-overlay/", "bx-shol", $completeContent);
    $completeContent = preg_replace("/_description/", "_des", $completeContent);
    $completeContent = preg_replace("/e_social_media_follow/", "e_smf", $completeContent);
    $completeContent = preg_replace("/e_audio_module_content/", "e_admc", $completeContent);
    $completeContent = preg_replace("/e_audio_cover_art/", "e_adca", $completeContent);
    $completeContent = preg_replace("/e_post_slider/", "e_ps", $completeContent);
    $completeContent = preg_replace("/custom-row/", "c-rw", $completeContent);
    $completeContent = preg_replace("/e_subscribe/", "e-sbc", $completeContent);
    $completeContent = preg_replace("/e_slide_title/", "e_s_t", $completeContent);
    $completeContent = preg_replace("/e_slider/", "e_sl", $completeContent);
    $completeContent = preg_replace("/e_signup_/", "e_su_", $completeContent);
    $completeContent = preg_replace("/e_slide_content/", "e_s_c", $completeContent);
    $completeContent = preg_replace("/e_divider/", "e_dr", $completeContent);
    $completeContent = preg_replace("/es_parallax/", "e_plx", $completeContent);
    
    $completeContent = preg_replace("/e_smf_network_/", "e_m_n_", $completeContent);
    $completeContent = preg_replace("/e_testimonial/", "e_tmn", $completeContent);
    $completeContent = preg_replace("/efh_container/", "eh_cr", $completeContent);
    $completeContent = preg_replace("/-webkit-box-sizing:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/display:-webkit-box;/", "", $completeContent);
    $completeContent = preg_replace("/display:-moz-box;/", "", $completeContent);
    $completeContent = preg_replace("/display:-ms-flexbox;/", "", $completeContent);
    $completeContent = preg_replace("/display:-webkit-flex;/", "", $completeContent);
    $completeContent = preg_replace("/display:-moz-flex;/", "", $completeContent);
    $completeContent = preg_replace("/display:-ms-flex;/", "", $completeContent);

    $completeContent = preg_replace("/-o-animation-timing-function:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-ms-animation-timing-function(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-animation-name:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-animation-name:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-ms-animation-name:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-o-animation-name:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-transition:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-transition:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-font-smoothing:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-osx-font-smoothing:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-box-sizing:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-border-radius:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-border-radius:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-background-size:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-background-size:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-animation-timing-function:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-animation-timing-function:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-animation-duration:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-animation-duration:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-o-animation-duration:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-animation-delay:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-animation-delay:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-ms-animation-delay:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-o-animation-delay:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-animation-fill-mode:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-animation-fill-mode:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-ms-animation-fill-mode:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-o-animation-fill-mode:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-justify-content:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/webkit-align-self:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-flex-flow:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-webkit-flex-flow:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-ms-justify-content:(.*?);/", "", $completeContent);
    $completeContent = preg_replace("/-moz-justify-content:(.*?);/", "", $completeContent);
    //$completeContent = preg_replace('/layout="intrinsic"/', 'layout="responsive"' , $completeContent);
    //$completeContent = preg_replace("/z-index:9;position:relative/", "", $completeContent);
//end here
    $completeContent = preg_replace('/<amp-img(.*?)srcset=""(.*?)><\/amp-img>/', '<amp-img$1$2></amp-img>' , $completeContent);
    

        $completeContent = apply_filters("amp_pc_divi_css_sorting", $completeContent);
        
        //$completeContent = preg_replace("/\.clearfix\{ clear:both \}/", "", $completeContent);
        
        return $completeContent;
    }


    function ampforwp_body_class_divi($classes){
        
        if(function_exists('et_layout_body_class') || function_exists('et_divi_theme_body_class') || function_exists('et_add_wp_version')) {
        $classes = et_layout_body_class($classes);
        $classes = et_divi_theme_body_class($classes);
        $classes = et_add_wp_version($classes);
        }
        $key = array_search('et_fixed_nav', $classes);
        $classes = array_filter($classes);
        $classes = array_unique($classes);
        if(isset($classes[$key])){
            unset($classes[$key]);
        }
        $key = array_search('single-post', $classes);
        if(isset($classes[$key])){
            unset($classes[$key]);
        }
        return $classes;
    }
    
    function amp_pbc_load_fonts(){
        if(defined('AMP_WC_PLUGIN_URI')){?>
            @font-face{font-family:star;src:url("<?php echo AMP_WC_PLUGIN_URI.'/assets/fonts/star.eot';?>");src:url("<?php echo AMP_WC_PLUGIN_URI.'/assets/fonts/star.eot?#iefix';?>") format('embedded-opentype'),url("<?php echo AMP_WC_PLUGIN_URI.'/assets/fonts/star.woff';?>") format('woff'),url("<?php echo AMP_WC_PLUGIN_URI.'/assets/fonts/star.ttf';?>") format('truetype'),url("<?php echo AMP_WC_PLUGIN_URI.'/assets/fonts/star.svg#star';?>") format('svg');font-weight:400;font-style:normal}
            @font-face{font-family:WooCommerce;src:url("<?php echo AMP_WC_PLUGIN_URI.'/assets/fonts/WooCommerce.eot';?>");src:url("<?php echo AMP_WC_PLUGIN_URI.'/assets/fonts/WooCommerce.eot?#iefix';?>") format('embedded-opentype'),url("<?php echo AMP_WC_PLUGIN_URI.'/assets/fonts/WooCommerce.woff';?>") format('woff'),url("<?php echo AMP_WC_PLUGIN_URI.'/assets/fonts/WooCommerce.ttf';?>") format('truetype'),url("<?php echo AMP_WC_PLUGIN_URI.'/assets/fonts/WooCommerce.svg#WooCommerce';?>") format('svg');font-weight:400;font-style:normal};
    <?php
        }
    }

    public function amp_divi_custom_styles(){
        global $post, $wp_styles;
        //From DIVI
        global $shortname;
        if(is_object($post)){
        $postID = $post->ID;
        }
         if ( function_exists('ampforwp_is_front_page') && ampforwp_is_front_page() ) {
            $postID = ampforwp_get_frontpage_id();
        }
        if ( 'on' !== get_post_meta( $postID, '_et_pb_use_builder', true ) ) {
            return ;
        }
        //require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR.'amp_vc_shortcode_styles.php';
        $css = '';
        $srcs = array();
        $style_suffix  = function_exists('et_load_unminified_styles') && et_load_unminified_styles() && ! is_child_theme() ? '.dev' : '';
        if(is_child_theme()){
            $parent_theme_css = get_template_directory_uri() . '/style' . $style_suffix . '.css';
            if (function_exists('et_is_builder_plugin_active') &&
                ! et_is_builder_plugin_active() && 
                is_child_theme() 
                ){
                $parent_theme_css = get_template_directory_uri() . '/style.dev.css';
            }
            $srcs[] = $parent_theme_css;
        }
        $srcs[] = get_stylesheet_directory_uri() . '/style' . $style_suffix . '.css';
        if( function_exists('ampwcpro_layout_selector') && ampwcpro_layout_selector() == 'v2layout' ){
        if ( !defined('DIR') ) { define('DIR', dirname(__FILE__)); }
        $srcs[] =  plugin_dir_url(DIR).'woocommerce/assets/css/woocommerce-layout.css';
        $srcs[] =  plugin_dir_url(DIR).'woocommerce/assets/css/woocommerce.css?ver=3.7.0';
    }        
    if(class_exists('AMP_ET_Builder_Module_Countdown_Timer')){
        $srcs[] =  plugin_dir_url(DIR).'amp-pagebuilder-compatibility/assets/style.css';}
        if(!empty(pagebuilder_for_amp_utils::get_setting_data('diviCssKeys') ) ){
            
            $update_css = pagebuilder_for_amp_utils::get_setting_data('diviCssKeys');
            $csslinks = explode(",", $update_css);
            if(count($csslinks)){
                $csslinks = array_filter($csslinks);
                $srcs = array_merge($srcs, $csslinks);
            }
        }
        $srcs[] = $this->getPostPageSpecificCss($post, $postID); 
        foreach( $wp_styles->queue as $style ) :
            $src = $wp_styles->registered[$style]->src;
            if(filter_var($src, FILTER_VALIDATE_URL) === FALSE){
                continue;
            }
            $srcs[$style] = $src;
        endforeach;

        if(count($srcs)>0){
            $srcs = array_unique($srcs);
        }

        if(is_array($srcs) && count($srcs)){
            foreach ($srcs as $key => $valuesrc) {
                $valuesrc = trim($valuesrc);
                if( filter_var($valuesrc, FILTER_VALIDATE_URL) === FALSE ){
                    continue;
                }
                $cssData = '';
                $cssData = $this->ampforwp_remote_content($valuesrc);              
                $cssData = preg_replace("/\/\*(.*?)\*\//si", "", $cssData);
                $css .= preg_replace_callback('/url[(](.*?)[)]/', function($matches)use($valuesrc){
                    $matches[1] = str_replace(array('"', "'"), array('', ''), $matches[1]);
                        if(!wp_http_validate_url($matches[1]) && strpos($matches[1],"data:")===false){
                            $urlExploded = explode("/", $valuesrc);
                            $parentUrl = str_replace(end($urlExploded), "", $valuesrc);
                            return 'url('.$parentUrl."/".$matches[1].")"; 
                        }else{
                            return $matches[0];
                        }
                    }, $cssData);
            }
        }
        $custom_css = et_get_option( "{$shortname}_custom_css" );
        if ( !empty( $custom_css ) ){
            $css .=  $custom_css;
        }
        $stylesContent = ET_Builder_Element::get_style() . ET_Builder_Element::get_style( true );
        if ( et_core_is_builder_used_on_current_request() ) {
            $stylesContent .= et_pb_get_page_custom_css();
        }
        $css .= $stylesContent;
        if(!empty(pagebuilder_for_amp_utils::get_setting_data('diviCss-custom') ) ){
            $css .= pagebuilder_for_amp_utils::get_setting_data('diviCss-custom');
        }
        if( function_exists('wp_get_custom_css') ){
            $css .= wp_get_custom_css();
        }

        $css = str_replace(array(" img", " video", "!important"), array(" amp-img", " amp-video", ""), $css);

        //For et module amp
        $css = preg_replace(
                            array(
                                '/url\(core\/admin\/fonts\/modules.eot\)/si',
                                '/url\(core\/admin\/fonts\/modules.eot\?#iefix\)/si',
                                '/url\(core\/admin\/fonts\/modules.woff\)/si',
                                '/url\(core\/admin\/fonts\/modules.svg#ETmodules\)/si',

                                ), 
                            array(
                                'url('.get_template_directory_uri().'/core/admin/fonts/modules.eot'.')',
                                'url('.get_template_directory_uri().'/core/admin/fonts/modules.eot?#iefix'.')',
                                'url('.get_template_directory_uri().'/core/admin/fonts/modules.woff'.')',
                                'url('.get_template_directory_uri().'/core/admin/fonts/modules.svg#ETmodules'.')',
                            ), $css);
        $css = preg_replace_callback('/url[(](.*?)[)]/', function($matches){
            $matches[1] = str_replace(array('"', "'"), array('', ''), $matches[1]);
            if(!wp_http_validate_url($matches[1]) && strpos($matches[1],"data:")===false){
                return 'url('.get_template_directory_uri()."/".$matches[1].")"; 
            }else{
                return $matches[0];
            }
        }, $css);
        echo $css.'.e_gallery_fullwidth .e_gallery_item:first-child {position: absolute;
}.e_gallery_0 {width: 83%;max-width: 72%;}.e_gallery_0.e_gallery amp-carousel {max-width: 88%;}@media screen and (max-width: 600px) {.e_gallery .e_gallery_items {max-width: 145%;}.e_gallery_0 {width: 100%;max-width: 100%;}.e_slide_with_image .e_sd{width:100%;}}.e_sl .e_slide{margin-right:0%;}';
    }

    public function ampforwp_remote_content($src){
        if($src){
            $arg = array( "sslverify" => false, "timeout" => 60 ) ;
            $response = wp_remote_get( $src, $arg );
            if ( wp_remote_retrieve_response_code($response) == 200 && is_array( $response ) ) {
              $header = wp_remote_retrieve_headers($response); // array of http header lines
              $contentData =  wp_remote_retrieve_body($response); // use the content
              return $contentData;
            }else{
                $contentData = file_get_contents( $src );
                if(! $contentData ){
                    $data = str_replace(get_site_url(), '', $src);//content_url()
                    $data = getcwd().$data;
                    if(file_exists($data)){
                        $contentData = file_get_contents($data);
                    }
                }
                return $contentData;
            }

        }
        return '';
    }

    public function getPostPageSpecificCss($post, $post_id){
        //From DIVI
        global $shortname;
        ET_Core_PageResource::startup();

        $post_id  = $post_id ? $post_id : et_core_page_resource_get_the_ID();

        $is_preview     = is_preview() || isset( $_GET['et_pb_preview_nonce'] ) || is_customize_preview(); // phpcs:ignore WordPress.Security.NonceVerification.NoNonceVerification
        $is_singular    = function_exists('et_core_page_resource_is_singular')? et_core_page_resource_is_singular() : false;

        $disabled_global = 'off' === et_get_option( 'et_pb_static_css_file', 'on' );
        $disabled_post   = $disabled_global || ( $is_singular && 'off' === get_post_meta( $post_id, '_et_pb_static_css_file', true ) );

        $forced_inline     = $is_preview || $disabled_global || $disabled_post || post_password_required();
        $builder_in_footer = 'on' === et_get_option( 'et_pb_css_in_footer', 'off' );

        $unified_styles = $is_singular && ! $forced_inline && ! $builder_in_footer && et_core_is_builder_used_on_current_request();
        $unified_styles = true;
        $owner = $unified_styles ? 'core' : $shortname;
        $slug  = $unified_styles ? 'unified' : 'customizer';
        if ( function_exists( 'et_fb_is_enabled' ) && et_fb_is_enabled() ) {
            $slug .= '-vb';
        }


        $slug           = sanitize_text_field( $slug );
        $global         = 'global' === $post_id ? '-global' : '';
        $filename = "et-{$owner}-{$slug}{$global}";

        $file_extension = '.min.css';
        $absolute_path  = ET_Core_PageResource::get_cache_directory();
        $relative_path  = ET_Core_PageResource::get_cache_directory( 'relative' );
        $files = glob( $absolute_path . "/{$post_id}/{$filename}-[0-9]*{$file_extension}" );

        if ( $files ) {
            // Static resource file exists
            $file           = array_pop( $files );
            $PATH     = ET_Core_PageResource::$data_utils->normalize_path( $file );
            $BASE_DIR = dirname( $PATH );

            $start     = strpos( $PATH, 'cache/et' );
            $URL = content_url( substr( $PATH, $start ) );

            if ( $files ) {
                // Somehow there are multiple files for this resource. Let's delete the extras.
                foreach ( $files as $extra_file ) {
                    ET_Core_Logger::debug( 'Removing extra page resource file: ' . $extra_file );
                    @ET_Core_PageResource::$wpfs->delete( $extra_file );
                }
            }

        } else {
            // Static resource file doesn't exist
            $time = (string) microtime( true );
            $time = str_replace( '.', '', $time );

            $relative_path .= "/{$post_id}/{$filename}-{$time}{$file_extension}";
            $absolute_path .= "/{$post_id}/{$filename}-{$time}{$file_extension}";

            $BASE_DIR = ET_Core_PageResource::$data_utils->normalize_path( dirname( $absolute_path ) );
            $TEMP_DIR = $BASE_DIR . "/{$slug}~";
            $PATH     = $absolute_path;
            $URL      = content_url( $relative_path );
        }

        return $URL;
    }

    public function amp_divi_pagebuilder_font_link(){
        global $post;
        if(is_object($post)){
        $postID = $post->ID;
        }
         if ( function_exists('ampforwp_is_front_page') && ampforwp_is_front_page() ) {
            $postID = ampforwp_get_frontpage_id();
        }
        $divi_enabled = false;
        if ( 'on' === get_post_meta( $postID, '_et_pb_use_builder', true ) ) {
            $divi_enabled = true;
        }
        if($divi_enabled && pagebuilder_for_amp_utils::get_setting_data('divi_fontawesome_support')==1 ){ ?>

        <link rel='stylesheet' id='font-awesome-css'  href='https://use.fontawesome.com/releases/v5.8.1/css/all.css' type='text/css' media='all' />
        <?php }    
        $this->et_builder_preprint_font();
        if (function_exists('et_divi_fonts_url')){
        $fontUrl = et_divi_fonts_url();
        }else{
           $fontUrl= '';   
        }
        if($fontUrl && !class_exists('\ElementorPro\Plugin')){
        echo '<link rel="stylesheet" id="divi-fonts-css" href="'.esc_url( str_replace("http:", "https:", $fontUrl)).'">';
        }
    }
    
    public function et_builder_preprint_font() {
        // Return if this is not a post or a page
        if(function_exists('et_core_use_google_fonts')){
            if ( ! is_singular() || ! et_core_use_google_fonts() ) {
                return;
            }   
        }else{
            if ( ! is_singular() ){
                return;
            }
        }

        $post_id = get_the_ID();

        $post_fonts_data = get_post_meta( $post_id, 'et_enqueued_post_fonts', true );

        // No need to proceed if the proper data is missing from the cache
        if ( ! is_array( $post_fonts_data ) || ! isset( $post_fonts_data['family'], $post_fonts_data['subset'] ) ) {
            return;
        }

        $fonts = $post_fonts_data[ 'family'];

        if ( ! $fonts ) {
            return;
        }

        $unique_subsets = $post_fonts_data[ 'subset'];
        $protocol       = 'https';

        echo '<link rel="stylesheet" id="et-builder-googlefonts-cached" href="'.esc_url( add_query_arg( array(
            'family' => implode( '|', $fonts ) ,
            'subset' => implode( ',', $unique_subsets ),
        ), "$protocol://fonts.googleapis.com/css" ) ).'">';
    }

    public function amp_divi_pagebuidler_override(){
        if ( (function_exists( 'ampforwp_is_amp_endpoint' ) && ampforwp_is_amp_endpoint()) ||  (function_exists( 'is_wp_amp' ) && is_wp_amp()) || (function_exists( 'is_amp_endpoint' ) && is_amp_endpoint()) ) {
        
            if ( class_exists( 'ET_Builder_Module' ) ) {
                $filesArray = array('Accordion.php', 'AmpNumberCounter.php','BarCounters.php', 'BarCountersItem.php', 'CountdownTimer.php',  'Map.php', 'FullwidthMap.php' , 'Video.php', 'ContactFormItem.php', 'ContactForm.php', 'Tabs.php', 'TabsItem.php', 'FilterablePortfolio.php','Toggle.php','Code.php','NumberCounters.php', 'signup.php', 'slider.php', 'fullwidthpostslider.php','Button.php','shop.php','Gallery.php','Blog.php','Fullwidthslideramp.php','Blurb.php','CircleCounter.php');//'MapItem.php',
                foreach ($filesArray as $key => $value) {
                    if(file_exists(AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR."/includes/divi/".$value)){
                        require_once AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR."/includes/divi/".$value;
                    }
                }
            }
        }
    }

    public static function et_builder_convert_line_breaks( $content, $line_breaks_format = "\n"  ) {

    // before we swap out the placeholders,
    // remove all the <p> tags and \n that wpautop added!
    $content = preg_replace( '/\n/smi', '', $content );
    $content = preg_replace( '/<p>/smi', '', $content );
    $content = preg_replace( '/<\/p>/smi', '', $content );

    $content = str_replace( array( '<!– [et_pb_line_break_holder] –>', '<!-- [et_pb_line_break_holder] -->', '||et_pb_line_break_holder||' ), $line_breaks_format, $content );
    $content = str_replace( '<!–- [et_pb_br_holder] -–>', '<br />', $content );

    // convert the <pee tags back to <p
    // see et_pb_prep_code_module_for_wpautop()
    $content = str_replace( '<pee', '<p', $content );
    $content = str_replace( '</pee>', '</p> ', $content );

    return $content;
}
    
}



/**
* Admin section portal Access
**/
add_action('plugins_loaded', 'pagebuilder_for_amp_divi_option');
function pagebuilder_for_amp_divi_option(){
    if(is_admin()){
        new pagebuilder_for_amp_divi_Admin();
    }else{
        // Instantiate AMP_PC_Divi_Pagebuidler.
        $diviAmpBuilder = new AMP_PC_Divi_Pagebuidler();
    }
    if ( defined( 'DOING_AJAX' ) ) {
        AMP_PC_Divi_Pagebuidler::load_ajax_calls();
    }
    
}
Class pagebuilder_for_amp_divi_Admin{
    function __construct(){
        add_filter( 'redux/options/redux_builder_amp/sections', array($this, 'add_options_for_divi'),7,1 );
    }
        public static function get_admin_options_divi($section = array()){
        $obj = new self();
        //print_r($obj);die;
        $section = $obj->add_options_for_divi($section);
        return $section;
    }
    function add_options_for_divi($sections){
        $desc = 'Enable/Activate Divi pagebuilder';
        $theme = wp_get_theme(); // gets the current theme
        if ( is_plugin_active( 'divi-builder/divi-builder.php' ) || 'Divi' == $theme->name || 'Divi' == $theme->parent_theme ) {
            $desc = '';
        }
       // print_r( $sections[3]['fields']);die;
        $accordionArray = array();
        $sectionskey = 0;
        foreach ($sections as $sectionskey => $sectionsData) {
            if($sectionsData['id']=='amp-content-builder' &&  count($sectionsData['fields'])>0 ){
                foreach ($sectionsData['fields'] as $fieldkey => $fieldvalue) {
                    if($fieldvalue['id'] == 'ampforwp-divi-pb-for-amp-accor'){
                        $accordionArray = $sections[$sectionskey]['fields'][$fieldkey];
                         unset($sections[$sectionskey]['fields'][$fieldkey]);
                    }
                    if($fieldvalue['id'] == 'ampforwp-divi-pb-for-amp'){
                        unset($sections[$sectionskey]['fields'][$fieldkey]);
                    }
                }
                break;
            }
        }
        $sections[$sectionskey]['fields'][] = $accordionArray;
        $sections[$sectionskey]['fields'][] = array(
                               'id'       => 'pagebuilder-for-amp-divi-support',
                               'type'     => 'switch',
                               'title'    => esc_html__('AMP Divi Compatibility ','accelerated-mobile-pages'),
                               'tooltip-subtitle' => esc_html__('Enable or Disable the Divi for AMP', 'accelerated-mobile-pages'),
                               'desc'     => $desc,
                               'section_id' => 'amp-content-builder',
                               'default'  => false
                            );
        foreach ($this->amp_divi_fields() as $key => $value) {
            $sections[$sectionskey]['fields'][] = $value;
        }
        

        return $sections;

    }

    public function amp_divi_fields(){
        $contents[] = array(
                        'id'       => 'diviCssKeys',
                        'type'     => 'textarea',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Enter css url', 'amp-pagebuilder-compatibility'),
                        'subtitle'  => esc_html__('', 'amp-pagebuilder-compatibility'),
                        'default'  => '',
                        'desc'      => esc_html__( 'Add your css url in comma saperated', 'amp-pagebuilder-compatibility' ),
                       // 'required'=> array(array('pagebuilder-for-amp-wpbakery-support','==', 1)),
                         'section_id' => 'amp-content-builder',

                    );
        $contents[] = array(
                        'id'       => 'diviCss-custom',
                        'type'     => 'textarea',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Enter custom css', 'amp-pagebuilder-compatibility'),
                        'subtitle'  => esc_html__('', 'amp-pagebuilder-compatibility'),
                        'default'  => '',
                        'desc'      => esc_html__( 'Add your custom css code', 'amp-pagebuilder-compatibility' ),
                         'section_id' => 'amp-content-builder',
                    );

        $contents[] = array(
                        'id'       => 'divi_fontawesome_support',
                        'type'     => 'switch',
                        'class'    => 'child_opt child_opt_arrow',
                        'title'    => esc_html__('Load fontawesome', 'amp-pagebuilder-compatibility'),
                        'desc'      => esc_html__( 'Load fontawesome library from CDN', 'amp-pagebuilder-compatibility' ),
                        'default'  => 0,
                        //'required'=> array(array('pagebuilder-for-amp-wpbakery-support','==', 1)),
                         'section_id' => 'amp-content-builder',
                    );
        return $contents;
    }
}