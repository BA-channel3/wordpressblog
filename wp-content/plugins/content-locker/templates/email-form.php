 
<form class="mts-cl-subscription-form">

    <div class="mts-cl-subscription-form-inner-wrap">

        <div class="mts-cl-field mts-cl-field-email">
            <div class="mts-cl-field-control">
                <input type="email" class="mts-cl-input mts-cl-input-required" id="mts-cl-input-email" placeholder="Por favor ingresa tu correo electónico" autocomplete="off" name="email">
            </div>
            <?php if(cl()->settings->get( 'trans_consent_input_placeholder')) { ?>
                <div class="mts-cl-field-control mts-cl-consent-control">
                    <input type="checkbox" class="mts-cl-input-required" id="mts-cl-input-consent" name="consent" required>
                    <label for="mts-cl-input-consent"><?php echo cl()->settings->get( 'trans_consent_input_placeholder'); ?></label>
                </div>
            <?php } ?>
        </div>
 
        <div class="mts-cl-field mts-cl-field-submit">
            <button class="mts-cl-button mts-cl-form-button mts-cl-submit">Ingresar para desbloquear contenido</button>
         </div>

    </div>

</form>
