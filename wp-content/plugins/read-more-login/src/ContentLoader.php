<?php
/** Read-More-Login plugin for WordPress.
 *  Puts a login/registration form in your posts and pages.
 *
 *  Copyright (C) 2018 Arild Hegvik
 *
 *  GNU GENERAL PUBLIC LICENSE (GNU GPLv3)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ARU_ReadMoreLogin;

defined( 'ABSPATH' ) || exit;

defined( 'ABSPATH' ) || exit;

use WP_PluginFramework\Utils\SecurityFilter;
use WP_PluginFramework\Utils\DebugLogger;

class ContentLoader
{
    static function FilterContent($content)
    {
        $modified_content = '';

        if(is_singular())
        {
            $position = strpos($content, ReadMoreLoginPlugin::ARU_READMORELOGIN_SHORT_CODE);

            if ($position)
            {
                $post_id_val = get_the_ID();

                DebugLogger::WriteDebugNote('Load free protected content post_id=' . strval($post_id_val));

                $free_visitor_from_google = false;
                $must_login_first = false;

                $draw_free_content = false;
                $draw_login_form = false;
                $draw_protected_content_placeholder = false;
                $draw_all_content = false;

                $add_ajax_handling = false;
                $send_js_data = array();

                $member_options = get_option(SettingsAccessOptions::OPTION_NAME);

                if (isset($member_options[SettingsAccessOptions::GOOGLE_READ]) and $member_options[SettingsAccessOptions::GOOGLE_READ] == '1')
                {

                    if (isset($_SERVER['HTTP_USER_AGENT']))
                    {
                        $agent = SecurityFilter::SanitizeText($_SERVER['HTTP_USER_AGENT']);

                        $notice = "HTTP_USER_AGENT=" . $agent;
                        DebugLogger::WriteDebugNote($notice);

                        if (preg_match('/bot|crawl|slurp|spider|Google|Yahoo|msnbot/i', $agent))
                        {
                            DebugLogger::WriteDebugNote($notice, 'Search agent. Show all page.');
                            $free_visitor_from_google = true;
                        }

                    }

                    if (isset($_SERVER['HTTP_REFERER']))
                    {
                        $from_url = SecurityFilter::SanitizeText($_SERVER['HTTP_REFERER']);

                        $notice = "HTTP_REFERER=" . $from_url;
                        DebugLogger::WriteDebugNote($notice);

                        $domain = parse_url($from_url, PHP_URL_HOST);
                        $domain = explode('.', $domain);
                        if (count($domain) >= 2)
                        {
                            $notice = "domain=" . $domain[count($domain) - 2];
                            DebugLogger::WriteDebugNote($notice);

                            if (($domain[count($domain) - 2] == 'google') || ($domain[count($domain) - 2] == 'googleboot'))
                            {
                                DebugLogger::WriteDebugNote($notice, 'Google search. Show all page.');
                                $free_visitor_from_google = true;
                            }
                        }
                    }
                }

                if (!is_user_logged_in())
                {
                    $must_login_first = true;
                }

                if ($free_visitor_from_google)
                {
                    $draw_all_content = true;
                }
                else
                {
                    if ($must_login_first)
                    {
                        $draw_free_content = true;
                        $draw_login_form = true;
                        $draw_protected_content_placeholder = true;
                    }
                    else
                    {
                        $draw_all_content = true;
                    }
                }

                $controller = new ReadMoreLoginController();

                if ($draw_free_content)
                {
                    $free_content = substr($content, 0, $position);

                    $modified_content .= $controller->GetFreeContentHtmlPart($free_content);
                }

                if ($draw_login_form)
                {
                    $register_form = $controller->Draw();

                    if (isset($register_form))
                    {
                        /* A registration form is active and needs more attention */
                        $modified_content .= $register_form;
                    }

                    $add_ajax_handling = true;
                }

                if ($draw_protected_content_placeholder)
                {
                    $modified_content .= $controller->GetProtectedContentPlaceholder();
                }


                if ($draw_all_content)
                {
                    $modified_content .= str_replace(ReadMoreLoginPlugin::ARU_READMORELOGIN_SHORT_CODE, '', $content);
                }

                if ($add_ajax_handling)
                {
                    $nonce_string = 'aru_rml_load_rest_of_content-' . strval(get_the_ID());
                    $wp_nonce = wp_create_nonce($nonce_string);
                    $send_js_data['wp_nonce'] = $wp_nonce;
                    $send_js_data['post_id'] = get_the_ID();
                }
            }
        }

        if ($modified_content == '')
        {
            return $content;
        }
        else
        {
            return $modified_content;
        }
    }

    static function PageLoadInit()
    {
        if (isset($_GET['rml']))
        {
            $controller = new ReadMoreLoginController();
            $reg_type = $controller->GetRegType();
            if ($reg_type == RegistrationDbTable::REG_TYPE_READ_MORE_REGISTRATION)
            {
                $controller->InitHandler('InitLogin');
            }
            elseif ($reg_type == RegistrationDbTable::REG_TYPE_USER_REGISTRATION)
            {
                $controller = new RegistrationController();
                $controller->InitHandler('Registering');
            }
        }
    }
}
