<?php
/** Read-More-Login plugin for WordPress.
 *  Puts a login/registration form in your posts and pages.
 *
 *  Copyright (C) 2018 Arild Hegvik
 *
 *  GNU GENERAL PUBLIC LICENSE (GNU GPLv3)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ARU_ReadMoreLogin;

defined( 'ABSPATH' ) || exit;

use WP_PluginFramework\HtmlComponents\StatusBar;

class AdminSettingsStyleController extends AdminSettingsPreviewFormController
{
    /** @var AdminStyleView */
    protected $View;

    public function __construct()
    {
        parent::__construct('ARU_ReadMoreLogin\SettingsStyleOptions', 'ARU_ReadMoreLogin\AdminStyleView');

        $this->TabName = 'style';
    }

    public function HandleSave($data_record)
    {
        return parent::HandleSave($data_record);

        /*
        if(parent::HandleSave($data_record))
        {
            $css_file = $data_record[SettingsStyleOptions::CSS_FILE];

            $style_url = plugin_dir_path(__DIR__) . 'css/rml_custom_style.css';
            return file_put_contents($style_url, $css_file);
        }
        else
        {
            return false;
        }
        */
    }

    public function HandleSaveSuccess($data_record)
    {
        $this->View->AdminStatusBar->SetStatusText(esc_html__('Your settings have been saved.', 'read-more-login'), StatusBar::STATUS_SUCCESS);

        parent::HandleSaveSuccess($data_record);

        if($this->View->fade_enable->GetValue())
        {
            $this->View->UpdateClientDom('div#rml_fade_content', 'remove');
            $fade_div = $this->PreviewController->GetFadeCover();
            $this->View->UpdateClientDom('div#rml_free_content', 'append', $fade_div);
        }
        else
        {
            $this->View->UpdateClientDom('div#rml_fade_content', 'remove');
        }

        $style2 = array();
        $style2[] = 'background-color';
        $style2[] = $this->View->fade_background_color->GetValue();
        $this->View->UpdateClientDom('div.rml_design_studio', 'css', $style2);
    }
}
